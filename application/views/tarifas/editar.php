<br>
<div class="container">
    <div class="card">
        <h4>
            <center>
                <br><b>
                    edit Tipo tarifas
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("/Tarifas/Actuaizar") ?>" method="post">
            <input value="<?php echo $tarifa->id_tar ?>" type="text" class="form-control" name="id_tar" id="id_tar" hidden aria-describedby="helpId" placeholder="" />
    
            <div class="row">
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="nombre_tar" class="form-label">Nombre</label>
                            <input value="<?php echo $tarifa->nombre_tar ?>" type="text" class="form-control" name="nombre_tar" id="nombre_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="descripcion_tar" class="form-label">descripcion</label>
                            <input value="<?php echo $tarifa->descripcion_tar ?>" type="text" class="form-control" name="descripcion_tar" id="descripcion_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="estado_tar" class="form-label">estado</label>
                            <select class="form-select form-select" name="estado_tar" id="estado_tar">
                                <option value="<?php echo $tarifa->estado_tar ?>" selected><?php echo $tarifa->estado_tar ?></option>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="m3_maximo_tar" class="form-label">Maxima</label>
                            <input value="<?php echo $tarifa->m3_maximo_tar ?>" type="text" class="form-control" name="m3_maximo_tar" id="m3_maximo_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_basica_tar" class="form-label">tarifa basica</label>
                            <input value="<?php echo $tarifa->tarifa_basica_tar ?>" step="any" type="number" class="form-control" name="tarifa_basica_tar" id="tarifa_basica_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_excedente_tar" class="form-label">tarifa ecxedente</label>
                            <input value="<?php echo $tarifa->tarifa_excedente_tar ?>" step="any" type="number" class="form-control" name="tarifa_excedente_tar" id="tarifa_excedente_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="valor_mora_tar" class="form-label">mora</label>
                            <input value="<?php echo $tarifa->valor_mora_tar ?>" step="any" type="number" class="form-control" name="valor_mora_tar" id="valor_mora_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo site_url("/Tarifas/index"); ?>" class="btn btn-dark">Cancelar</a>

            </center>
        </div>

            </form>
        </div>
    </div>

</div>