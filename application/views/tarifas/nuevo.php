<br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
        <h4>
            <center>
                <br><b>
                    new Tipo Event
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("/Tarifas/guardar") ?>" method="post">
                <div class="row">
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="nombre_tar" class="form-label">Nombre</label>
                            <input type="text" class="form-control" name="nombre_tar" id="nombre_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="descripcion_tar" class="form-label">descripcion</label>
                            <input type="text" class="form-control" name="descripcion_tar" id="descripcion_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="estado_tar" class="form-label">estado</label>
                            <select class="form-select form-select" name="estado_tar" id="estado_tar">
                                <option selected>Select one</option>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="m3_maximo_tar" class="form-label">Maxima</label>
                            <input type="text" class="form-control" name="m3_maximo_tar" id="m3_maximo_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_basica_tar" class="form-label">tarifa basica</label>
                            <input step="any" type="number" class="form-control" name="tarifa_basica_tar" id="tarifa_basica_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_excedente_tar" class="form-label">tarifa ecxedente</label>
                            <input step="any" type="number" class="form-control" name="tarifa_excedente_tar" id="tarifa_excedente_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="valor_mora_tar" class="form-label">mora</label>
                            <input step="any" type="number" class="form-control" name="valor_mora_tar" id="valor_mora_tar" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url("/Tarifas/index"); ?>" class="btn btn-danger">Cancel</a>

            </center>
        </div>

            </form>
        </div>
    </div>
</div>
</div>
