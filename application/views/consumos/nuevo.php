<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
<h4>
            <center>
                <br><b>
                    New Consumption
                </b>

            </center>
            </h4>
    <div class="card">

        <div class="card-body">
        <form action="<?php echo site_url("/consumos/guardar") ?>" method="post">
        <div class="row">
          <div class="col-md-4">
                  <label for="">YEAR: <span </span></label>
                  <br>
                  <input type="number" class="form-control" required name="anio_consumo" value="" id="anio_consumo" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">MONTH: <span </span></label>
                  <br>
                  <input type="text" class="form-control" required name="mes_consumo" value="" id="mes_consumo" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">STATE: <span </span></label>
                  <br>
                  <select class="form-control" required name="estado_consumo" id="estado_consumo" style="background-color: white;"required>
                      <option value="CERRADO">CERRADO</option>
                      <option value="ABIERTO">ABIERTO</option>
                  </select>
             </div>
              <div class="col-md-4">
                  <label for="">CREATION DATE: <span </span></label>
                  <br>
                  <input type="datetime-local" class="form-control" required name="fecha_creacion_consumo" value="" id="fecha_creacion_consumo" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">DATE UPDATE: <span </span></label>
                  <br>
                  <input type="datetime-local" class="form-control" required name="fecha_actualizacion_consumo" value="" id="fecha_actualizacion_consumo" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">MONTH NUMBER: <span </span></label>
                  <br>
                  <select class="form-control" required name="numero_mes_consumo" id="numero_mes_consumo" style="background-color: white;"required>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select>
              </div>
              <div class="col-md-4">
                  <label for="">EXPIRATION DATE: <span </span></label>
                  <br>
                  <input type="date" class="form-control" required name="fecha_vencimiento_consumo" value="" id="fecha_vencimiento_consumo" style="background-color: white;"required>
              </div>
        </div>

        <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url(); ?>/consumos/index" class="btn btn-danger">Cancel</a>

            </center>
        </div>
    </form>
        </div>
    </div>

</div>
</div>
</div>
