<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container">
<h4>
            <center>
                <br><b>
                    Edit Consumption
                </b>

            </center>
            </h4>
    <div class="card">

        <div class="card-body">
        <form action="<?php echo site_url("/consumos/procesarActualizacion/") ?>" method="post">
        <div class="row">
          <input type="hidden" name="id_consumo" id="id_consumo" value="<?php echo $consumoEditar->id_consumo; ?>">
          <div class="col-md-4">
                  <label for="">YEAR: <span class="obligatorio">(Obligatorio)</span></label>
                  <br>
                  <input type="number" class="form-control" required name="anio_consumo" value="<?php echo $consumoEditar->anio_consumo ?>" id="anio_consumo" style="background-color: white;">
              </div>
              <div class="col-md-4">
                  <label for="">MONTH: <span class="obligatorio">(Obligatorio)</span></label>
                  <br>
                  <input type="text" class="form-control" required name="mes_consumo" value="<?php echo $consumoEditar->mes_consumo ?>" id="mes_consumo" style="background-color: white;">
              </div>
              <div class="col-md-4">
                  <label for="">STATE: <span class="obligatorio">(Obligatorio)</span></label>
                  <br>
                  <select class="form-control" required name="estado_consumo" value="<?php echo $consumoEditar->estado_consumo ?>" id="estado_consumo" style="background-color: white;">
                      <option value="CERRADO">CERRADO</option>
                      <option value="ABIERTO">ABIERTO</option>
                  </select>
             </div>
              <div class="col-md-4">
                  <label for="">CREATION DATE: <span class="obligatorio">(Obligatorio)</span></label>
                  <br>
                  <input type="datetime-local" class="form-control" required name="fecha_creacion_consumo" value="<?php echo $consumoEditar->fecha_creacion_consumo ?>" id="fecha_creacion_consumo" style="background-color: white;">
              </div>
              <div class="col-md-4">
                  <label for="">DATE UPDATE: <span class="obligatorio">(Obligatorio)</span></label>
                  <br>
                  <input type="datetime-local" class="form-control" required name="fecha_actualizacion_consumo" value="<?php echo $consumoEditar->fecha_actualizacion_consumo ?>" id="fecha_actualizacion_consumo" style="background-color: white;">
              </div>
              <div class="col-md-4">
                  <label for="">MONTH NUMBER: <span class="obligatorio">(Obligatorio)</span></label>
                  <br>
                  <select class="form-control" required name="numero_mes_consumo" value="<?php echo $consumoEditar-> numero_mes_consumo ?>" id="numero_mes_consumo" style="background-color: white;">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                  </select>
              </div>
              <div class="col-md-4">
                  <label for="">EXPIRATION DATE: <span class="obligatorio">(Obligatorio)</span></label>
                  <br>
                  <input type="date" class="form-control" required name="fecha_vencimiento_consumo" value="<?php echo $consumoEditar-> fecha_vencimiento_consumo ?>" id="fecha_vencimiento_consumo" style="background-color: white;">
              </div>

        <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-primary">keep</button>
                <a href="<?php echo site_url(); ?>/consumos/index" class="btn btn-dark">Cancelar</a>

            </center>
        </div>
    </form>
        </div>
    </div>

</div>
