<br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
        <h4>
            <center>
                <br><b>
                    new partners
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("Socios/guardar") ?>" method="post">
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tipo_soc" class="form-label">Tipo</label>
                            <select class="form-select form-select" name="tipo_soc" id="tipo_soc">
                                <option selected>Select one</option>
                                <option value="NATURAL">NATURAL</option>
                                <option value="ADMIN">ADMIN</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="identificacion_soc" class="form-label">Identificacion</label>
                            <input type="number" class="form-control" name="identificacion_soc" id="identificacion_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="primer_apellido_soc" class="form-label">Primer Apellido</label>
                            <input type="text" class="form-control" name="primer_apellido_soc" id="primer_apellido_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <bs5 class="col-4">
                        <div class="mb-3">
                            <label for="segundo_apellido_soc" class="form-label">Segundo apellido</label>
                            <input type="text" class="form-control" name="segundo_apellido_soc" id="segundo_apellido_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </bs5>
                    <bs5 class="col-4">
                        <div class="mb-3">
                            <label for="nombres_soc" class="form-label">Nombres</label>
                            <input type="text" class="form-control" name="nombres_soc" id="nombres_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </bs5>
                    <bs5 class="col-4">
                        <div class="mb-3">
                            <label for="email_soc" class="form-label">Email</label>
                            <input type="email" class="form-control" name="email_soc" id="email_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </bs5>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="telefono_soc" class="form-label">Telefono</label>
                            <input type="number" class="form-control" name="telefono_soc" id="telefono_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="direccion_soc" class="form-label">Direccion</label>
                            <input type="text" class="form-control" name="direccion_soc" id="direccion_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="fecha_nacimiento_soc" class="form-label">Fecha Nacimiento</label>
                            <input type="date" class="form-control" name="fecha_nacimiento_soc" id="fecha_nacimiento_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="discapacidad_soc" class="form-label">Discapacidad</label>
                            <input type="text" class="form-control" name="discapacidad_soc" id="discapacidad_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_soc" class="form-label">Estado</label>
                            <select class="form-select form-select" name="estado_soc" id="estado_soc">
                                <option selected>Select one</option>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="fk_id_usu" class="form-label">Usuario</label>
                            <select class="form-select form-select" name="fk_id_usu" id="fk_id_usu">
                                <option selected>Select one</option>
                                <?php foreach ($usuario as $usu) { ?>
                                    <option value="<?php echo $usu->id_usu ?>"><?php echo $usu->nombre_usu ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
                <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url("/Socios/index"); ?>" class="btn btn-danger">Cancel</a>

            </center>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
