<br>
<div class="container">
    <div class="card">
        <h4>
            <center>
                <br><b>
                    Edit partners
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("Socios/Actualizar") ?>" method="post">
            
            <input value="<?php echo $socio->id_soc ?>" hidden type="number" class="form-control" name="id_soc" id="id_soc" aria-describedby="helpId" placeholder="" />
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tipo_soc" class="form-label">Tipo</label>
                            <select class="form-select form-select" name="tipo_soc" id="tipo_soc">
                                <option selected value="<?php echo $socio->tipo_soc ?>"><?php echo $socio->tipo_soc ?></option>
                                <option value="NATURAL">NATURAL</option>
                                <option value="ADMIN">ADMIN</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="identificacion_soc" class="form-label">Identificacion</label>
                            <input value="<?php echo $socio->identificacion_soc ?>" type="number" class="form-control" name="identificacion_soc" id="identificacion_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="primer_apellido_soc" class="form-label">Primer Apellido</label>
                            <input value="<?php echo $socio->primer_apellido_soc ?>" type="text" class="form-control" name="primer_apellido_soc" id="primer_apellido_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <bs5 class="col-4">
                        <div class="mb-3">
                            <label for="segundo_apellido_soc" class="form-label">Segundo apellido</label>
                            <input value="<?php echo $socio->segundo_apellido_soc ?>" type="text" class="form-control" name="segundo_apellido_soc" id="segundo_apellido_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </bs5>
                    <bs5 class="col-4">
                        <div class="mb-3">
                            <label for="nombres_soc" class="form-label">Nombres</label>
                            <input value="<?php echo $socio->nombres_soc ?>" type="text" class="form-control" name="nombres_soc" id="nombres_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </bs5>
                    <bs5 class="col-4">
                        <div class="mb-3">
                            <label for="email_soc" class="form-label">Email</label>
                            <input value="<?php echo $socio->email_soc ?>" type="email" class="form-control" name="email_soc" id="email_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </bs5>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="telefono_soc" class="form-label">Telefono</label>
                            <input value="<?php echo $socio->telefono_soc ?>" type="number" class="form-control" name="telefono_soc" id="telefono_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="direccion_soc" class="form-label">Direccion</label>
                            <input value="<?php echo $socio->direccion_soc ?>" type="text" class="form-control" name="direccion_soc" id="direccion_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="fecha_nacimiento_soc" class="form-label">Fecha Nacimiento</label>
                            <input value="<?php echo $socio->fecha_nacimiento_soc ?>" type="date" class="form-control" name="fecha_nacimiento_soc" id="fecha_nacimiento_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="discapacidad_soc" class="form-label">Discapacidad</label>
                            <input value="<?php echo $socio->discapacidad_soc ?>" type="text" class="form-control" name="discapacidad_soc" id="discapacidad_soc" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_soc" class="form-label">Estado</label>
                            <select class="form-select form-select" name="estado_soc" id="estado_soc">
                                <option value="<?php echo $socio->estado_soc ?>" selected></option>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="fk_id_usu" class="form-label">Usuario</label>
                            <select class="form-select form-select" name="fk_id_usu" id="fk_id_usu">
                                <option value="<?php echo $socio->fk_id_usu ?>" selected></option>
                                <?php foreach ($usuario as $usu) { ?>
                                    <option value="<?php echo $usu->id_usu ?>"><?php echo $usu->nombre_usu ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
                <center>
                <br>
                <button type="submit" class="btn btn-primary">keep</button>
                <a href="<?php echo site_url("/Socios/index"); ?>" class="btn btn-dark">Cancelar</a>

            </center>
                </div>
            </form>
        </div>
    </div>

</div>