<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
<h4>
            <center>
                <br><b>
                    new perfil
                </b>

            </center>
            </h4>
    <div class="card">

        <div class="card-body">
        <form action="<?php echo site_url("/Perfiles/guardar") ?>" method="post">
        <div class="row">
            <div class="col-4">
                <div class="mb-3">
                    <label for="nombre_per" class="form-label">Nombre</label>
                    <input type="text" class="form-control" name="nombre_per" id="nombre_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label for="descripcion_per" class="form-label">Descrip</label>
                    <input type="text" class="form-control" name="descripcion_per" id="descripcion_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label for="estado_per" class="form-label">Estado</label>
                    <select class="form-select form-select-lg" name="estado_per" id="estado_per">
                        <option selected>Sleccione una</option>
                        <option value="Activo">Activo</option>
                        <option value="Inactivo">Inactivo</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="creacion_per" class="form-label">Creacion</label>
                    <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="creacion_per" id="creacion_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="actualizacion_per" class="form-label">Actualizacion</label>
                    <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="actualizacion_per" id="actualizacion_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
        </div>
        <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url(); ?>/Perfiles/index" class="btn btn-danger">Cancel</a>

            </center>
        </div>
    </form>
        </div>
    </div>

</div>
</div>
</div>
