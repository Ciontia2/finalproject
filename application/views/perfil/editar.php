<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container">
    <div class="card">
    <center>
    <h3>
            <center>
                <br><b>
                    edit perfil
                </b>

            </center>
            </h3>
        <div class="card-body">

    <form action="<?php echo site_url("/Perfiles/Actualizar") ?>" method="post">
        <div class="row">

        <input value="<?php echo $editPer->id_per ?>" hidden type="text" class="form-control" name="id_per" id="id_per" aria-describedby="helpId" placeholder="" />

            <div class="col-4">
                <div class="mb-3">
                    <label for="nombre_per" class="form-label">Nombre</label>
                    <input value="<?php echo $editPer->nombre_per ?>" type="text" class="form-control" name="nombre_per" id="nombre_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label for="descripcion_per" class="form-label">Descrip</label>
                    <input value="<?php echo $editPer->descripcion_per ?>" type="text" class="form-control" name="descripcion_per" id="descripcion_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
            <div class="col-4">
                <div class="mb-3">
                    <label for="estado_per" class="form-label">Estado</label>
                    <select class="form-select form-select-lg" name="estado_per" id="estado_per">
                        <option selected><?php echo $editPer->estado_per ?></option>
                        <option value="Activo">Activo</option>
                        <option value="Inactivo">Inactivo</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="mb-3">
                    <label for="creacion_per" class="form-label">Creacion</label>
                    <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="creacion_per" id="creacion_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="actualizacion_per" class="form-label">Actualizacion</label>
                    <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="actualizacion_per" id="actualizacion_per" aria-describedby="helpId" placeholder="" />
                </div>

            </div>
        </div>
        <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-primary">keep</button>
                <a href="<?php echo site_url(); ?>/Perfiles/index" class="btn btn-dark">Cancelar</a>

            </center>
        </div>
    </form>
        </div>
    </div>

</div>
