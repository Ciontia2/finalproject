<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Aplicaci_Yaku</title>
  <!-- base:css -->
  <link rel="stylesheet" href="<?php echo base_url('plantilla/vendors/typicons/typicons.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('plantilla/vendors/css/vendor.bundle.base.css'); ?>">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('plantilla/css/vertical-layout-light/style.css') ?>">
  <!-- endinject -->
  <link rel="shortcut icon" href="images/favicon.png" />
  <!-- Importar CHART JS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@2.9.0/dist/chartjs-adapter-moment.bundle.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.0"></script>
  <script src="https://cdn.jsdelivr.net/npm/chartjs-3d@2.1.3"></script>

  <meta content="width=device-width, initial-scale=1.0" name="viewport">
<meta content="" name="keywords">
<meta content="" name="description">

<!-- Favicon -->


<!-- Google Web Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&family=Red+Rose:wght@600;700&display=swap"
rel="stylesheet">

<!-- Icon Font Stylesheet -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.4/font/bootstrap-icons.css">


<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

<!-- import of dataTables -->
<link rel="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css" href="/css/master.css">
<link rel="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js" href="/css/master.css">
<!--Importacion de jquey-->
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<!-- Importacion de datatables-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>




<!-- Favicon -->


<!-- Google Web Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&family=Red+Rose:wght@600;700&display=swap"
rel="stylesheet">

<!-- Icon Font Stylesheet -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.4/font/bootstrap-icons.css">

<!-- Libraries Stylesheet -->
f="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

<!-- import of dataTables -->
<link rel="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css" href="/css/master.css">
<link rel="https://cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js" href="/css/master.css">
<!--Importacion de jquey-->
<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
<!-- Importacion de datatables-->
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>







<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Aplicación Yaku</title>
<!-- Otros elementos meta y enlaces CSS van aquí -->
<style>
    body {
        background-color: #ADD8E6; /* Celeste */
    }
</style>




</head>
<<style>
    body {
        background-color: #ADD8E6; /* Celeste */
    }
</style>>
  <div class="row" id="proBanner">
    <div class="col-12">
      <span class="d-flex align-items-center purchase-popup">
        <p>Technical University of Cotopaxi - Information Systems</p>
        <a href="https://bootstrapdash.com/demo/polluxui/template/index.html?utm_source=organic&utm_medium=banner&utm_campaign=free-preview" target="_blank" class="btn download-button purchase-button ml-auto">Upgrade To Pro</a>
        <i class="typcn typcn-delete-outline" id="bannerClose"></i>
      </span>
    </div>
  </div>
  <div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="navbar-brand-wrapper d-flex justify-content-center">
        <div class="navbar-brand-inner-wrapper d-flex justify-content-between align-items-center w-100">
          <a class="navbar-brand brand-logo" href="index.html"><img src="images/logo.svg" alt="logo"/></a>
          <a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/logo-mini.svg" alt="logo"/></a>
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="typcn typcn-th-menu"></span>
          </button>
        </div>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
        <ul class="navbar-nav mr-lg-2">
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link" href="#" data-toggle="dropdown" id="profileDropdown">
              <img src="images/faces/face5.jpg" alt="profile"/>
              <span class="nav-profile-name">Eugenia Mullins</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item">
                <i class="typcn typcn-cog-outline text-primary"></i>
                Settings
              </a>
              <a class="dropdown-item">
                <i class="typcn typcn-eject text-primary"></i>
                Logout
              </a>
            </div>
          </li>
          <li class="nav-item nav-user-status dropdown">
              <p class="mb-0">Last login was 23 hours ago.</p>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item nav-date dropdown">
            <a class="nav-link d-flex justify-content-center align-items-center" href="javascript:;">
              <h6 class="date mb-0">Today : Mar 23</h6>
              <i class="typcn typcn-calendar"></i>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
              <i class="typcn typcn-cog-outline mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="images/faces/face4.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-normal">DAVID GREY
                  </h6>
                  <p class="font-weight-light small-text text-muted mb-0">
                    The meeting is cancelled
                  </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="images/faces/face2.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-normal">Tim Cook
                  </h6>
                  <p class="font-weight-light small-text text-muted mb-0">
                    New product launch
                  </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-normal"> Johnson
                  </h6>
                  <p class="font-weight-light small-text text-muted mb-0">
                    Upcoming board meeting
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown mr-0">
            <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="typcn typcn-bell mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="typcn typcn-info mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal">Application Error</h6>
                  <p class="font-weight-light small-text mb-0 text-muted">
                    Just now
                  </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="typcn typcn-cog-outline mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal">Settings</h6>
                  <p class="font-weight-light small-text mb-0 text-muted">
                    Private message
                  </p>
                </div>
              </a>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="typcn typcn-user mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal">New user registration</h6>
                  <p class="font-weight-light small-text mb-0 text-muted">
                    2 days ago
                  </p>
                </div>
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="typcn typcn-th-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
       <nav class="navbar-breadcrumb col-xl-12 col-12 d-flex flex-row p-0">
         <div class="navbar-links-wrapper d-flex align-items-stretch">
           <div class="nav-link">
             <a href="javascript:;"><i class="typcn typcn-calendar-outline"></i></a>
           </div>
           <div class="nav-link">
             <a href="javascript:;"><i class="typcn typcn-mail"></i></a>
           </div>
           <div class="nav-link">
             <a href="javascript:;"><i class="typcn typcn-folder"></i></a>
           </div>
           <div class="nav-link">
             <a href="javascript:;"><i class="typcn typcn-document-text"></i></a>
           </div>
         </div>
         <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
           <ul class="navbar-nav mr-lg-2">
             <li class="nav-item ml-0">
               <h4 class="mb-0">Dashboard</h4>
             </li>
             <li class="nav-item">
               <div class="d-flex align-items-baseline">
                 <p class="mb-0">Home</p>
                 <i class="typcn typcn-chevron-right"></i>
                 <p class="mb-0">Main Dahboard</p>
               </div>
             </li>
           </ul>
           <ul class="navbar-nav navbar-nav-right">
             <li class="nav-item nav-search d-none d-md-block mr-0">
               <div class="input-group">
                 <input type="text" class="form-control" placeholder="Search..." aria-label="search" aria-describedby="search">
                 <div class="input-group-prepend">
                   <span class="input-group-text" id="search">
                     <i class="typcn typcn-zoom"></i>
                   </span>
                 </div>
               </div>
             </li>
           </ul>
         </div>
       </nav>
       <div class="container-fluid page-body-wrapper">
         <!-- partial:partials/_settings-panel.html -->
         <div class="theme-setting-wrapper">
           <div id="settings-trigger"><i class="typcn typcn-cog-outline"></i></div>
           <div id="theme-settings" class="settings-panel">
             <i class="settings-close typcn typcn-times"></i>
             <p class="settings-heading">SIDEBAR SKINS</p>
             <div class="sidebar-bg-options selected" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Light</div>
             <div class="sidebar-bg-options" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark</div>
             <p class="settings-heading mt-2">HEADER SKINS</p>
             <div class="color-tiles mx-0 px-4">
               <div class="tiles success"></div>
               <div class="tiles warning"></div>
               <div class="tiles danger"></div>
               <div class="tiles info"></div>
               <div class="tiles dark"></div>
               <div class="tiles default"></div>
             </div>
           </div>
         </div>
         <div id="right-sidebar" class="settings-panel">
           <i class="settings-close typcn typcn-times"></i>
           <ul class="nav nav-tabs" id="setting-panel" role="tablist">
             <li class="nav-item">
               <a class="nav-link active" id="todo-tab" data-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" id="chats-tab" data-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
             </li>
           </ul>
           <div class="tab-content" id="setting-content">
             <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
               <div class="add-items d-flex px-3 mb-0">
                 <form class="form w-100">
                   <div class="form-group d-flex">
                     <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                     <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
                   </div>
                 </form>
               </div>
               <div class="list-wrapper px-3">
                 <ul class="d-flex flex-column-reverse todo-list">
                   <li>
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox">
                         Team review meeting at 3.00 PM
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li>
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox">
                         Prepare for presentation
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li>
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox">
                         Resolve all the low priority tickets due today
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li class="completed">
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox" checked>
                         Schedule meeting for next week
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                   <li class="completed">
                     <div class="form-check">
                       <label class="form-check-label">
                         <input class="checkbox" type="checkbox" checked>
                         Project review
                       </label>
                     </div>
                     <i class="remove typcn typcn-delete-outline"></i>
                   </li>
                 </ul>
               </div>
               <div class="events py-4 border-bottom px-3">
                 <div class="wrapper d-flex mb-2">
                   <i class="typcn typcn-media-record-outline text-primary mr-2"></i>
                   <span>Feb 11 2018</span>
                 </div>
                 <p class="mb-0 font-weight-thin text-gray">Creating component page</p>
                 <p class="text-gray mb-0">build a js based app</p>
               </div>
               <div class="events pt-4 px-3">
                 <div class="wrapper d-flex mb-2">
                   <i class="typcn typcn-media-record-outline text-primary mr-2"></i>
                   <span>Feb 7 2018</span>
                 </div>
                 <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                 <p class="text-gray mb-0 ">Call Sarah Graves</p>
               </div>
             </div>
             <!-- To do section tab ends -->
             <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
               <div class="d-flex align-items-center justify-content-between border-bottom">
                 <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
                 <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See All</small>
               </div>
               <ul class="chat-list">
                 <li class="list active">
                   <div class="profile"><img src="images/faces/face1.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Thomas Douglas</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">19 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
                   <div class="info">
                     <div class="wrapper d-flex">
                       <p>Catherine</p>
                     </div>
                     <p>Away</p>
                   </div>
                   <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                   <small class="text-muted my-auto">23 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face3.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Daniel Russell</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">14 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
                   <div class="info">
                     <p>James Richardson</p>
                     <p>Away</p>
                   </div>
                   <small class="text-muted my-auto">2 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face5.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Madeline Kennedy</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">5 min</small>
                 </li>
                 <li class="list">
                   <div class="profile"><img src="images/faces/face6.jpg" alt="image"><span class="online"></span></div>
                   <div class="info">
                     <p>Sarah Graves</p>
                     <p>Available</p>
                   </div>
                   <small class="text-muted my-auto">47 min</small>
                 </li>
               </ul>
             </div>
             <!-- chat tab ends -->
           </div>
         </div>
         <!-- partial -->
         <!-- partial:partials/_sidebar.html -->
         <nav class="sidebar sidebar-offcanvas" id="sidebar">
           <ul class="nav">
             <li class="nav-item">
               <a class="nav-link" href="<?php echo site_url("dashboard/index") ?>">
                 <i class="typcn typcn-device-desktop menu-icon"></i>
                 <span class="menu-title">Dashboard</span>
                 <div class="badge badge-danger">new</div>
               </a>
             </li>



             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables1" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-user-add"> </i>
                 <span class="menu-title">Assists</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables1">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Asistencias/index") ?>">Assistance List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables1">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Asistencias/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables2" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-phone-outline"> </i>
                 <span class="menu-title">Announcements</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables2">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("/Comunicados/index") ?>">List of Announcements</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables2">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("/Comunicados/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables3" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-starburst"> </i>
                 <span class="menu-title">Settings</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables3">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Configuracions/index") ?>">Settings list</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables3">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Configuracions/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables4" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-ticket"> </i>
                 <span class="menu-title">Consumption</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables4">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("/Consumos/index") ?>">Consumption List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables4">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("/Consumos/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables5" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-thumbs-ok"> </i>
                 <span class="menu-title">Details</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables5">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("/Detalles/index") ?>">Details List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables5">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("/Detalles/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables6" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-wine"> </i>
                 <span class="menu-title">Events</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables6">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("/Eventos/index") ?>">Events List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables6">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Eventos/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables7" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-world"> </i>
                 <span class="menu-title">Surpluses</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables7">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Excedentes/index") ?>">Surpluses List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables7">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Excedentes/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables8" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-user-add-outline"> </i>
                 <span class="menu-title">Owner History</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables8">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Historial_propietarios/index") ?>">Owner History List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables8">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Historial_propietarios/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables9" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-refresh-outline"> </i>
                 <span class="menu-title">Taxes</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables9">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Impuestos/index") ?>">Taxes List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables9">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Impuestos/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables10" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-printer"> </i>
                 <span class="menu-title">Readings</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables10">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Lecturas/index") ?>">Readings List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables10">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Lecturas/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables11" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-pipette"> </i>
                 <span class="menu-title">Gauges</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables11">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Medidors/index") ?>">Gauges List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables11">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Medidors/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables12" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-group"> </i>
                 <span class="menu-title">Profiles</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables12">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Perfiles/index") ?>">Profiles List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables12">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Perfiles/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables13" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-shopping-cart"> </i>
                 <span class="menu-title">Collections</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables13">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Recaudaciones/index") ?>">Collections List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables13">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Recaudaciones/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables14" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-location-outline"> </i>
                 <span class="menu-title">Routes</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables14">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Rutas/index") ?>">Routes List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables14">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Rutas/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables15" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-group-outline"> </i>
                 <span class="menu-title">Partners</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables15">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Socios/index") ?>">Partners List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables15">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Socios/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables16" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-film"> </i>
                 <span class="menu-title">Rates</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables16">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Tarifas/index") ?>">Rates List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables16">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Tarifas/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables17" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-chart-pie-outline"> </i>
                 <span class="menu-title">Type of Events</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables17">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("TipoEventos/index") ?>">Event Type list</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables17">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("TipoEventos/nuevo") ?>">New</a></li>
                 </ul>
               </div>
             </li>

             <li class="nav-item">
               <a class="nav-link" data-toggle="collapse" href="#tables18" aria-expanded="false" aria-controls="tables">
                 <i class="typcn typcn-user-outline"> </i>
                 <span class="menu-title">Users</span>
                 <i class="menu-arrow"></i>
               </a>
               <div class="collapse" id="tables18">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Usuarios/index") ?>">Users List</a></li>
                 </ul>
               </div>
               <div class="collapse" id="tables18">
                 <ul class="nav flex-column sub-menu">
                   <li class="nav-item"> <a class="nav-link" href="<?php echo site_url("Usuarios/nuevo") ?>">New</a></li>
                 </ul>
               </div>

             </li>
















           </ul>
         </nav>
         <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">

          <div class="row">
            <div class="col-xl-6 grid-margin stretch-card flex-column">
                <h5 class="mb-2 text-titlecase mb-4">Status statistics</h5>


          </div>
