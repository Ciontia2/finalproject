<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="contenedor1">
    <h4>
        <center>
            <br><b>
                Edit reading
            </b>

        </center>
    </h4>
    <form class="" id="frm_nuevo_lectura" action="<?php echo site_url("lecturas/procesarActualizacion"); ?>"
        method="post">
        <div class="container">
            <div class="row">
                <input type="hidden" name="id_lec" id="id_lec" value="<?php echo $lecturaEditar->id_lec; ?>">
                <div class="col-md-4">
                    <label for="">YEAR: <span lectura</span></label>
                    <br>
                    <input type="text" class="form-control" required name="anio_lec"
                        value="<?php echo $lecturaEditar->anio_lec; ?>" id="anio_lec" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">MONTH: <span lectura</span></label>
                    <br>
                    <input type="text" class="form-control" required name="mes_lec"
                        value="<?php echo $lecturaEditar->mes_lec; ?>" id="mes_lec" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">STATE: <span lectura</span></label>
                    <br>
                    <select class="form-control" required name="estado_lec"
                        value="<?php echo $lecturaEditar->estado_lec; ?>" id="estado_lec"
                        style="background-color: white;">
                        <option value="INGRESADA">INGRESADA</option>
                        <option value="COBRADA">COBRADA</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">PREVIOUS READING: <span lectura</span></label>
                    <br>
                    <input type="number" class="form-control" required name="lectura_anterior_lec"
                        value="<?php echo $lecturaEditar->lectura_anterior_lec; ?>" id="lectura_anterior_lec"
                        style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CURRENT READING: <span lectura</span></label>
                    <br>
                    <input type="number" class="form-control" required name="lectura_actual_lec"
                        value="<?php echo $lecturaEditar->lectura_actual_lec; ?>" id="lectura_actual_lec"
                        style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CREATION DATE: <span lectura</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="fecha_creacion_lec"
                        value="<?php echo $lecturaEditar->fecha_creacion_lec; ?>" id="fecha_creacion_lec"
                        style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">UPDATE DATE: <span lectura</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="fecha_actualizacion_lec"
                        value="<?php echo $lecturaEditar->fecha_actualizacion_lec; ?>" id="fecha_actualizacion_lec"
                        style="background-color: white;">
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_his" class="form-label">Historial propietarios</label>
                            <select class="form-select form-select" name="fk_id_his" id="fk_id_his">
                                <option value="<?php echo $lecturaEditar->fk_id_his ?>" selected><?php echo $lecturaEditar->fk_id_his ?></option>
                                <?php foreach ($historial as $registro) { ?>
                                    <option value="<?php echo $registro->id_his ?>">
                                        <?php echo $registro->actualizacion_his ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_consumo" class="form-label">Historial propietarios</label>
                            <select class="form-select form-select" name="fk_id_consumo" id="fk_id_consumo">
                                <option value="<?php echo $lecturaEditar->fk_id_consumo ?>" selected><?php echo $lecturaEditar->fk_id_consumo ?></option>
                                <?php foreach ($Consumo as $registro) { ?>
                                    <option value="<?php echo $registro->id_consumo ?>">
                                        <?php echo $registro->anio_consumo . " " . $registro->mes_consumo ?>/option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    keep
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/lecturas/index" class="btn btn-dark">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div>