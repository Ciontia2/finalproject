<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
    <h4>
        <center>
            <br><b>
                New reading
            </b>

        </center>
    </h4>
    <form class="" id="frm_nuevo_lectura" action="<?php echo site_url("lecturas/guardar"); ?>" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <label for="">YEAR: <span lectura</span></label>
                    <br>
                    <input type="text" class="form-control" required name="anio_lec" value="" id="anio_lec" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">MONTH: <span lectura</span></label>
                    <br>
                    <input type="text" class="form-control" required name="mes_lec" value="" id="mes_lec" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">STATE: <span lectura</span></label>
                    <br>
                    <select class="form-control" required name="estado_lec" value="" id="estado_lec" style="background-color: white;">
                        <option value="INGRESADA">INGRESADA</option>
                        <option value="COBRADA">COBRADA</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">PREVIOUS READING: <span lectura</span></label>
                    <br>
                    <input type="number" class="form-control" required name="lectura_anterior_lec" value="" id="lectura_anterior_lec" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CURRENT READING: <span lectura</span></label>
                    <br>
                    <input type="number" class="form-control" required name="lectura_actual_lec" value="" id="lectura_actual_lec" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CREATION DATE: <span lectura</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="fecha_creacion_lec" value="" id="fecha_creacion_lec" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">UPDATE DATE: <span lectura</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="fecha_actualizacion_lec" value="" id="fecha_actualizacion_lec" style="background-color: white;">
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_his" class="form-label">Historial propietarios</label>
                            <select class="form-select form-select" name="fk_id_his" id="fk_id_his">
                                <option selected>Select one</option>
                                <?php foreach($historial as $registro){ ?>
                                <option value="<?php echo $registro->id_his ?>"><?php echo $registro->	actualizacion_his ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_consumo" class="form-label">Historial propietarios</label>
                            <select class="form-select form-select" name="fk_id_consumo" id="fk_id_consumo">
                                <option selected>Select one</option>
                                <?php foreach($Consumo as $registro){ ?>
                                <option value="<?php echo $registro->id_consumo ?>"><?php echo $registro->anio_consumo ." ".$registro->mes_consumo ?>/option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-success">
                    keep
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/lecturas/index" class="btn btn-danger">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div>
</div>
</div>
