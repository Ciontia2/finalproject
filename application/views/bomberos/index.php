
<!-- ******************************************* CANVA ACTIVIDAD 3 ********************************************* -->
<div class="container">
  <h3><center>TOTAL VISITS BY MONTH</center></h3>
  <h5>Actividad 3</h5>
</div>
<div class="row">
        <!-- Año 2020 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2020 -->
                    <div class="card-body" style="background-color: #99FEF3; height: 90px;">
                        <p class="card-text">YEAR 2020</p>
                        <h5 class="card-title">
                            <?php echo $List2020; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2020 -->
                    <canvas id="lineChart2020" width="300" height="300"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2021 -->
        <div class="col-md-3 mb-7">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2021 -->
                    <div class="card-body" style="background-color: #99FEA1; height: 90px;">
                        <p class="card-text">YEAR 2021</p>
                        <h5 class="card-title">
                            <?php echo $List2021; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2021 -->
                    <canvas id="lineChart2021" width="200" height="200"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2022 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2022 -->
                    <div class="card-body" style="background-color: #D999FE; height: 90px;">
                        <p class="card-text">YEAR 2022</p>
                        <h5 class="card-title">
                            <?php echo $List2022; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2022 -->
                    <canvas id="lineChart2022" width="200" height="200"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2023 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2023 -->
                    <div class="card-body" style="background-color: #FF7456; height: 90px;">
                        <p class="card-text">YEAR 2023</p>
                        <h5 class="card-title">
                            <?php echo $List2023; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2023 -->
                    <canvas id="lineChart2023" width="200" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ******************************************* CANVA ACTIVIDAD 4 ********************************************* -->

<div class="container">
  <h3><center>TOTAL NOTIFICATIONS</center></h3>
  <h5>Actividad 4</h5>
</div>
<div class="row">
        <!-- Año 2020 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2020 -->
                    <div class="card-body" style="background-color: #99FEF3; height: 90px;">
                        <p class="card-text">Total notifications by code</p>
                        <h5 class="card-title">
                         <?php echo $totalNotificacionesTop10; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2020 -->
                    <canvas id="bar1" width="300" height="300"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2021 -->
        <div class="col-md-3 mb-7">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2021 -->
                    <div class="card-body" style="background-color: #99FEA1; height: 90px;">
                        <p class="card-text">Total Notifications by Status</p>
                        <h5 class="card-title">
                        <?php echo $totalEstados; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2021 -->
                    <canvas id="pie2" width="200" height="200"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2022 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2022 -->
                    <div class="card-body" style="background-color:  #D999FE; height: 90px;">
                        <p class="card-text">Notifications by Type of Activity and Year</p>
                        <h5 class="card-title">
                <?php echo $totalNotificaciones; // Aquí debes obtener la variable $totalNotificaciones en tu controlador ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2022 -->
                    <canvas id="line" width="200" height="200"></canvas>
                </div>
            </div>
        </div>

        <!-- Año 2023 -->
        <div class="col-md-3">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="height: 450px; width: 350px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2023 -->
                    <div class="card-body" style="background-color: #FF7456; height: 90px;">
                        <p class="card-text">Total Notifications by State and Month</p>
                        <h5 class="card-title">
                          <?php echo $totalSolicitudes; ?>
                        </h5>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2023 -->
                    <canvas id="pie" width="200" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- LIZ -->

<!-- ******************************************* CANVA ACTIVIDAD 5 ********************************************* -->
<div class="container">
  <h3><center>TOTAL MESSAGES</center></h3>
  <h5>Actividad 5</h5>
</div>

<div class="row">

  <div class="col-md-3">
      <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
      <div class="card" style="height: 450px; width: 350px;">
          <div class="card-body">

              <div class="card-body" style="background-color: #99FEF3; height: 90px;">
                  <p class="card-text">total number of calls per year</p>
                  <h5 class="card-title">
                   <?php echo $getTotalMensajes; ?>
                  </h5>
              </div>
              <!-- Gráfico 1: Cantidad de mensajes por año -->
              <canvas id="graficoMensajesPorAnio" width="400" height="400"></canvas>
          </div>
      </div>
  </div>

  <div class="col-md-3">
      <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
      <div class="card" style="height: 450px; width: 350px;">
          <div class="card-body">

              <div class="card-body" style="background-color: #99FEF3; height: 90px;">
                  <p class="card-text">total number of calls per year and month</p>
                  <h5 class="card-title">
                   <?php echo $getTotalMensajes; ?>
                  </h5>
              </div>
              <!-- Gráfico 2: Cantidad de mensajes por año y mes -->
              <canvas id="graficoMensajesPorAnioYMes" width="400" height="400"></canvas>

          </div>
      </div>
  </div>

  <div class="col-md-3">
      <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
      <div class="card" style="height: 450px; width: 350px;">
          <div class="card-body">

              <div class="card-body" style="background-color: #99FEF3; height: 90px;">
                  <p class="card-text">Total Gallery and Files</p>
                  <h5 class="card-title">
                   <?php echo $getTotalArchivosYGaleriasSumados; ?>
                  </h5>
              </div>
              <!-- Gráfico 3: Cantidad de archivos por tipo -->
              <canvas id="graficoCantidadArchivosPorTipo" width="400" height="400"></canvas>

          </div>
      </div>
  </div>


</div>



    <script type="text/javascript">
        // Gráfico 1: Cantidad de mensajes por año
        var datosMensajesPorAnio = {
            labels: [
                <?php foreach ($cantidadMensajesPorAnio as $dato) : ?>
                    '<?php echo $dato->anio_lot; ?>',
                <?php endforeach; ?>
            ],
            datasets: [{
                label: 'number of calls per year',
                data: [
                    <?php foreach ($cantidadMensajesPorAnio as $dato) : ?>
                        <?php echo $dato->cantidad_mensajes; ?>,
                    <?php endforeach; ?>
                ],
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)',
                  'rgba(54, 162, 235, 0.6)',
                  'rgba(255, 206, 86, 0.6)',
                  'rgba(75, 192, 192, 0.6)',
                ],
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            }]
        };

        var ctxMensajesPorAnio = document.getElementById('graficoMensajesPorAnio').getContext('2d');
        new Chart(ctxMensajesPorAnio, {
            type: 'bar',
            data: datosMensajesPorAnio,
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        // Gráfico 2: Cantidad de mensajes por año y mes
        var datosMensajesPorAnioYMes = {
            labels: [
                <?php foreach ($mensajesPorAnioYMes as $dato) : ?>
                    '<?php echo $dato->anio; ?>-<?php echo $dato->mes; ?>',
                <?php endforeach; ?>
            ],
            datasets: [{
                label: 'number of calls per year and per month',
                data: [
                    <?php foreach ($mensajesPorAnioYMes as $dato) : ?>
                        <?php echo $dato->cantidad_mensajes; ?>,
                    <?php endforeach; ?>
                ],
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)',
                  'rgba(54, 162, 235, 0.6)',
                  'rgba(255, 206, 86, 0.6)',
                  'rgba(75, 192, 192, 0.6)',
                ],
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        };

        var ctxMensajesPorAnioYMes = document.getElementById('graficoMensajesPorAnioYMes').getContext('2d');
        new Chart(ctxMensajesPorAnioYMes, {
            type: 'bar',
            data: datosMensajesPorAnioYMes,
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        // Gráfico 3: Cantidad de archivos por tipo
        var datosCantidadArchivosPorTipo = {
            labels: [
                'Files',
                'Gallery'
            ],
            datasets: [{
                label: 'Cantidad de archivos por tipo',
                data: [
                    <?php foreach ($cantidadArchivosPorTipo as $dato) : ?>
                        <?php echo $dato->cantidad_archivos; ?>,
                    <?php endforeach; ?>
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.6)',
                    'rgba(54, 162, 235, 0.6)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }]
        };

        var ctxCantidadArchivosPorTipo = document.getElementById('graficoCantidadArchivosPorTipo').getContext('2d');
        new Chart(ctxCantidadArchivosPorTipo, {
            type: 'doughnut',
            data: datosCantidadArchivosPorTipo,
            options: {}
        });
    </script>

<!-- CINTIA -->

<!-- ******************************************* CANVA ACTIVIDAD 6 ********************************************* -->
<!-- ANGEL -->



<!-- ****************************************** SCRIPT ACTIVIDAD 3 *******************************************-->
<!-- 2020 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2020): ?>

         <?php foreach ($getByVisit2020 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2020',
       data: [
         <?php if ($getByVisit2020): ?>

           <?php foreach ($getByVisit2020 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2020').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- 2021 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2021): ?>

         <?php foreach ($getByVisit2021 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2021',
       data: [
         <?php if ($getByVisit2021): ?>

           <?php foreach ($getByVisit2021 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2021').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- 2022 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2022): ?>

         <?php foreach ($getByVisit2022 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2022',
       data: [
         <?php if ($getByVisit2022): ?>

           <?php foreach ($getByVisit2022 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2022').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- 2023 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2023): ?>

         <?php foreach ($getByVisit2023 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2023',
       data: [
         <?php if ($getByVisit2023): ?>

           <?php foreach ($getByVisit2023 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 0.6)', // Color de la primera barra
         'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
         'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2023').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- ****************************************** SCRIPT ACTIVIDAD 4 *******************************************-->

<script type="text/javascript">
var datos = {
    labels: [
      <?php if ($bomberosByNotificacion): ?>
        <?php foreach ($bomberosByNotificacion as $bombero): ?>
          '<?php echo $bombero-> codigo_sol; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ],
    datasets: [{
      label: 'Datos de ejemplo',
      data: [
        <?php if ($bomberosByNotificacion): ?>
          <?php foreach ($bomberosByNotificacion as $bombero): ?>
            '<?php echo $bombero-> total_notificaciones; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ], // Valores de las barras
      backgroundColor: [
        'rgba(255, 99, 132, 0.6)', // Color de la primera barra
        'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
        'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('bar1').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'bar',
    data: datos,
    options: opciones
  });
</script>



<script type="text/javascript">
var datos = {
    labels: [
      <?php if ($bomberosByState): ?>
        <?php foreach ($bomberosByState as $bombero): ?>
          '<?php echo $bombero-> estado_sol; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ],
    datasets: [{
      label: 'Datos de ejemplo',
      data: [
        <?php if ($bomberosByState): ?>
          <?php foreach ($bomberosByState as $bombero): ?>
            '<?php echo $bombero-> porcentaje; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ], // Valores de las barras
      backgroundColor: [
        'rgba(255, 99, 132, 0.6)', // Color de la primera barra
        'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
        'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('pie2').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'doughnut',
    data: datos,
    options: opciones
  });
</script>


<script type="text/javascript">
var datos = {
    labels: [
      <?php if ($bomberosByActividad): ?>
        <?php foreach ($bomberosByActividad as $bombero): ?>
          '<?php echo $bombero-> actividad_sol; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ],
    datasets: [{
      label: 'Datos de ejemplo',
      data: [
        <?php if ($bomberosByActividad): ?>
          <?php foreach ($bomberosByActividad as $bombero): ?>
            '<?php echo $bombero-> count_notificaciones; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ], // Valores de las barras
      backgroundColor: [
        'rgba(255, 99, 132, 0.6)', // Color de la primera barra
        'rgba(54, 156, 235, 0.6)', // Color de la segunda barra
        'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('line').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'line',
    data: datos,
    options: opciones
  });
</script>

<script type="text/javascript">
var datos = {
    labels: [
        <?php if ($bomberosByMeses): ?>
            <?php foreach ($bomberosByMeses as $bombero): ?>
                '<?php echo $bombero->anio; ?>',
            <?php endforeach; ?>
        <?php endif; ?>
    ],
    datasets: [{
        label: 'Datos de ejemplo',
        data: [
            <?php if ($bomberosByMeses): ?>
                <?php foreach ($bomberosByMeses as $bombero): ?>
                    '<?php echo $bombero->porcentaje; ?>',
                <?php endforeach; ?>
            <?php endif; ?>
        ],
        backgroundColor: [
            'rgba(255, 99, 132, 0.6)', // Color de la primera barra
            'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
            'rgba(255, 206, 86, 0.6)', // Color de la tercera barra
            'rgba(75, 192, 192, 0.6)', // Color de la cuarta barra
            'rgba(153, 102, 255, 0.6)', // Color de la quinta barra
            'rgba(255, 159, 64, 0.6)' // Color de la sexta barra
        ],
        borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
    }]
};

// Opciones de configuración
var opciones = {
    scales: {
        y: {
            beginAtZero: true
        }
    }
};

// Obtener el contexto del lienzo
var contexto = document.getElementById('pie').getContext('2d');

// Crear el gráfico de barras
var graficoDeBarras = new Chart(contexto, {
    type: 'pie',
    data: datos,
    options: opciones
});
</script>

<!-- LIZ -->


<!-- ****************************************** SCRIPT ACTIVIDAD 5 *******************************************-->
<!-- CINTIA -->


<!-- ****************************************** SCRIPT ACTIVIDAD 6 *******************************************-->
<!-- ANGEL -->
