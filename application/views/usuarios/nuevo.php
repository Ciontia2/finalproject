








<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">

          <h4>
              <center>
                  <br><b>
                      New User
                  </b>

              </center>
              </h4>
          <div class="card-body">
              <form action="<?php echo site_url("/Usuarios/guardar") ?>" method="post">
                  <duv class="row">
                      <div class="col-4">
                          <div class="mb-3">
                              <label for="apellido_usu" class="form-label">Last name</label>
                              <input type="text" class="form-control" name="apellido_usu" id="apellido_usu" aria-describedby="helpId" placeholder="" />
                          </div>

                      </div>
                      <div class="col-4">
                          <div class="mb-3">
                              <label for="nombre_usu" class="form-label">Name</label>
                              <input type="text" class="form-control" name="nombre_usu" id="nombre_usu" aria-describedby="helpId" placeholder="" />
                          </div>

                      </div>
                      <div class="col-4">
                          <div class="mb-3">
                              <label for="email_usu" class="form-label">Email</label>
                              <input type="email" class="form-control" name="email_usu" id="email_usu" aria-describedby="helpId" placeholder="" />
                          </div>

                      </div>
                  </duv>
                  <duv class="row">
                      <div class="col-4">
                          <div class="mb-3">
                              <label for="password_usu" class="form-label">Password</label>
                              <input type="text" class="form-control" name="password_usu" id="password_usu" aria-describedby="helpId" placeholder="" />
                          </div>

                      </div>
                      <div class="col-4">
                          <div class="mb-3">
                              <label for="estado_usu" class="form-label">Status</label>
                              <select class="form-select form-select" name="estado_usu" id="estado_usu">
                                  <option selected>Select one</option>
                                  <option value="ACTIVE">ACTIVE</option>
                                  <option value="INACTIVE">INACTIVE</option>
                              </select>
                          </div>

                      </div>
                      <div class="col-4">
                          <div class="mb-3">
                              <label for="fk_id_per" class="form-label">Profile</label>
                              <select class="form-select form-select" name="fk_id_per" id="fk_id_per">
                                  <option selected>Select one</option>
                                  <?php foreach ($perfil as $registro) { ?>
                                      <option value="<?php echo $registro->id_per ?>"><?php echo $registro->nombre_per ?></option>
                                  <?php  } ?>

                              </select>
                          </div>

                      </div>
                      <div class="row">
                          <center>
                              <br>
                          <button class="btn btn-success" type="submit">Keep</button>
                          <a href="<?php echo site_url(); ?>/Usuarios/index" class="btn btn-danger">Cancel</a>


                          </center>
                      </div>

                  </duv>
              </form>
          </div>

        </div>
    </div>
</div>
