<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="contenedor1">
  <h4>
              <center>
                  <br><b>
                      EDIT DETAIL
                  </b>

              </center>
              </h4>
    <form class="" id="frm_nuevo_impuesto" action="<?php echo site_url("impuestos/guardar"); ?>" method="post">
        <div class="container">
            <div class="row">
              <input type="hidden" name="id_imp" id="id_imp" value="<?php echo $impuestoEditar->id_imp; ?>">
                <div class="col-md-4">
                    <label for="">NAME: <span </span></label>
                    <br>
                    <input type="text" class="form-control" required name="nombre_imp" value="<?php echo $impuestoEditar->nombre_imp ?>" id="nombre_imp" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">DESCRIPTION: <span </span></label>
                    <br>
                    <input type="text" class="form-control" required name="descripcion_imp" value="<?php echo $impuestoEditar->descripcion_imp ?>" id="descripcion_imp" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">PERCENTAGE: <span </span></label>
                    <br>
                    <input type="number" class="form-control" required name="porcentaje_imp" value="<?php echo $impuestoEditar->porcentaje_imp ?>" id="porcentaje_imp" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">RETENTION: <span </span></label>
                    <br>
                    <input type="number" class="form-control" required name="retencion_imp" value="<?php echo $impuestoEditar->retencion_imp ?>" id="retencion_imp" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">STATE: <span </span></label>
                    <br>
                    <input type="text" class="form-control" required name="estado_imp" value="<?php echo $impuestoEditar->estado_imp ?>" id="estado_imp" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CREATION: <span </span></label>
                    <br>
                    <input type="datetime" class="form-control" required name="creacion_imp" value="<?php echo $impuestoEditar->creacion_imp ?>" id="creacion_imp" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">UPDATE: <span </span></label>
                    <br>
                    <input type="datetime" class="form-control" required name="actualizacion_imp" value="<?php echo $impuestoEditar->actualizacion_imp ?>" id="actualizacion_imp" style="background-color: white;">
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    keep
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/impuestos/index" class="btn btn-dark">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div>
