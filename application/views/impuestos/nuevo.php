<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
  <h4>
              <center>
                  <br><b>
                      New Details
                  </b>

              </center>
              </h4>
    <form class="" id="frm_nuevo_impuesto" action="<?php echo site_url("impuestos/guardar"); ?>" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <label for="">NAME: <span </span></label>
                    <br>
                    <input type="text" class="form-control" required name="nombre_imp" value="" id="nombre_imp" style="background-color: white;"required>
                </div>
                <div class="col-md-4">
                    <label for="">DESCRIPTION: <span </span></label>
                    <br>
                    <input type="text" class="form-control" required name="descripcion_imp" value="" id="descripcion_imp" style="background-color: white;"required>
                </div>
                <div class="col-md-4">
                    <label for="">PERCENTAGE: <span </span></label>
                    <br>
                    <input type="number" class="form-control" required name="porcentaje_imp" value="" id="porcentaje_imp" style="background-color: white;"required>
                </div>
                <div class="col-md-4">
                    <label for="">RETENTION: <span </span></label>
                    <br>
                    <input type="number" class="form-control" required name="retencion_imp" value="" id="retencion_imp" style="background-color: white;"required>
                </div>
                <div class="col-md-4">
                    <label for="">STATE: <span </span></label>
                    <br>
                    <input type="text" class="form-control" required name="estado_imp" value="" id="estado_imp" style="background-color: white;"required>
                </div>
                <div class="col-md-4">
                    <label for="">CREATION: <span </span></label>
                    <br>
                    <input type="datetime" class="form-control" required name="creacion_imp" value="" id="creacion_imp" style="background-color: white;"required>
                </div>
                <div class="col-md-4">
                    <label for="">UPDATE: <span </span></label>
                    <br>
                    <input type="datetime" class="form-control" required name="actualizacion_imp" value="" id="actualizacion_imp" style="background-color: white;"required>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-success">
                    keep
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/impuestos/index" class="btn btn-danger">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div></div></div>
