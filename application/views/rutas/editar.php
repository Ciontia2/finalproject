<br>
<div class="container">
    <div class="card">
        <h4>
            <center>
                <br><b>
                    New Rutas
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("/Rutas/guardar") ?>" method="post">
            <input value="<?php echo $editRuta->id_rut ?>" hidden type="text" class="form-control" name="id_rut" id="id_rut" aria-describedby="helpId" placeholder="" />
    
            <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_rut" class="form-label">nombre</label>
                            <input value="<?php echo $editRuta->nombre_rut ?>" type="text" class="form-control" name="nombre_rut" id="nombre_rut" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="descripcion_rut" class="form-label">descripcion</label>
                            <input value="<?php echo $editRuta->descripcion_rut ?>" type="text" class="form-control" name="descripcion_rut" id="descripcion_rut" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_rut" class="form-label">estado</label>
                            <select class="form-select form-select-lg" name="estado_rut" id="estado_rut">
                                <option value="<?php echo $editRuta->estado_rut ?>" selected><?php echo $editRuta->estado_rut ?></option>
                                <option value="ACTIVO">AVTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                    <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-primary">keep</button>
                <a href="<?php echo site_url("/Rutas/index"); ?>" class="btn btn-dark">Cancelar</a>

            </center>
        </div>
                </div>
            </form>
        </div>
    </div>

</div>