<br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
        <h4>
            <center>
                <br><b>
                    New Rutas
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("/Rutas/guardar") ?>" method="post">
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="nombre_rut" class="form-label">nombre</label>
                            <input type="text" class="form-control" name="nombre_rut" id="nombre_rut" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="descripcion_rut" class="form-label">descripcion</label>
                            <input type="text" class="form-control" name="descripcion_rut" id="descripcion_rut" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="estado_rut" class="form-label">estado</label>
                            <select class="form-select form-select-lg" name="estado_rut" id="estado_rut">
                                <option selected>Select one</option>
                                <option value="ACTIVO">AVTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                    <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url("/Rutas/index"); ?>" class="btn btn-danger">Cancel</a>

            </center>
        </div>
                </div>
            </form>
        </div>
    </div>

</div></div>
