<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container">
    <h4>
        <center>
            <br><b>
                EDIT DETAILS
            </b>

        </center>
    </h4>
    <div class="card">

        <div class="card-body">
            <form action="<?php echo site_url("/detalles/procesarActualizacion") ?>" method="post">
                <div class="row">
                    <input value="<?php echo $detalleEditar->id_det ?>" hidden type="text" class="form-control" name="id_det" id="id_det" aria-describedby="helpId" placeholder="" />
                    <div class="col-md-4">
                        <label for="">AMOUNT: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="cantidad_det" value="<?php echo $detalleEditar->cantidad_det ?>" id="cantidad_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">DETAILS: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="detalle_det" value="<?php echo $detalleEditar->detalle_det ?>" id="detalle_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">WORTH: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="valor_unitario_det" value="<?php echo $detalleEditar->valor_unitario_det ?>" id="valor_unitario_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">SUBTOTAL: <span </span></label>
                        <br>
                        <input type="number" class="form-control" required name="subtotal_det" value="<?php echo $detalleEditar->subtotal_det ?>" id="subtotal_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">VAT: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="iva_det" value="<?php echo $detalleEditar->iva_det ?>" id="iva_det" style="background-color: white;" required>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_lec" class="form-label">Lectura</label>
                            <select class="form-select form-select" name="fk_id_lec" id="fk_id_lec">
                                <option value="<?php echo $detalleEditar->fk_id_lec ?>" selected><?php echo $detalleEditar->fk_id_lec ?></option>
                                <?php foreach ($Lectura as $registro) { ?>
                                    <option value="<?php echo $registro->id_lec ?>"><?php echo $registro->estado_lec ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_rec" class="form-label">Recaudacion</label>
                            <select class="form-select form-select" name="fk_id_rec" id="fk_id_rec">
                                <option value="<?php echo $detalleEditar->fk_id_rec ?>" selected><?php echo $detalleEditar->fk_id_rec ?></option>
                                <?php foreach ($Recaudacion as $registro) { ?>
                                    <option value="<?php echo $registro->id_rec ?>"><?php echo $registro->numero_factura_rec ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <center>
                        <br>
                        <button type="submit" class="btn btn-primary">keep</button>
                        <a href="<?php echo site_url(); ?>/detalles/index" class="btn btn-dark">Cancelar</a>

                    </center>
                </div>
            </form>
        </div>
    </div>

</div>