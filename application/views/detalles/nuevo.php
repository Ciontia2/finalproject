<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
    <h4>
        <center>
            <br><b>
                New Details
            </b>

        </center>
    </h4>
    <div class="card">

        <div class="card-body">
            <form action="<?php echo site_url("/detalles/guardar") ?>" method="post">
                <div class="row">
                    <div class="col-md-4">
                        <label for="">AMOUNT: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="cantidad_det" value="" id="cantidad_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">DETAILS: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="detalle_det" value="" id="detalle_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">WORTH: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="valor_unitario_det" value="" id="valor_unitario_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">SUBTOTAL: <span </span></label>
                        <br>
                        <input type="number" class="form-control" required name="subtotal_det" value="" id="subtotal_det" style="background-color: white;" required>
                    </div>
                    <div class="col-md-4">
                        <label for="">VAT: <span </span></label>
                        <br>
                        <input type="text" class="form-control" required name="iva_det" value="" id="iva_det" style="background-color: white;" required>
                    </div>
                </div>
                <div class="row justify-content-center align-items-center g-2">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_lec" class="form-label">Lectura</label>
                            <select class="form-select form-select" name="fk_id_lec" id="fk_id_lec">
                                <option selected>Select one</option>
                                <?php foreach ($Lectura as $registro) { ?>
                                    <option value="<?php echo $registro->id_lec ?>"><?php echo $registro->estado_lec ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_rec" class="form-label">Recaudacion</label>
                            <select class="form-select form-select-lg" name="fk_id_rec" id="fk_id_rec">
                                <option selected>Select one</option>
                                <?php foreach ($Recaudacion as $registro) { ?>
                                    <option value="<?php echo $registro->id_rec ?>"><?php echo $registro->numero_factura_rec ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <center>
                        <br>
                        <button type="submit" class="btn btn-success">keep</button>
                        <a href="<?php echo site_url(); ?>/detalles/index" class="btn btn-danger">Cancel</a>

                    </center>
                </div>
            </form>
        </div>
    </div>
</div></div>
</div>
