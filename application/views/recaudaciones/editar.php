<?php
date_default_timezone_set('America/Guayaquil');

?>


<br>
<div class="container">
    <div class="card">
        <h4>
            <center>
                <br><b>
                    editar recaudaciones
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("/Recaudaciones/Actucalizar") ?>" method="post">
            <input hidden value="<?php echo $Recaudacion->id_rec ?>" type="text" class="form-control" name="id_rec" id="id_rec" aria-describedby="helpId" placeholder="" />
    
            <div class="row">
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="numero_factura_rec" class="form-label">Numero Factura</label>
                            <input value="<?php echo $Recaudacion->numero_factura_rec ?>" type="text" class="form-control" name="numero_factura_rec" id="numero_factura_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="numero_autorizacion_rec" class="form-label">numero Actulaizacion</label>
                            <input value="<?php echo $Recaudacion->numero_autorizacion_rec ?>" type="text" class="form-control" name="numero_autorizacion_rec" id="numero_autorizacion_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="fecha_hora_autorizacion_rec" class="form-label">Fecha Actuarizacion</label>
                            <input value="<?php echo $Recaudacion->fecha_hora_autorizacion_rec ?>" value="<?php echo date('Y-m-d H:i:s'); ?>" type="datetime-local" class="form-control" name="fecha_hora_autorizacion_rec" id="fecha_hora_autorizacion_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="ambiente_rec" class="form-label">Ambiente</label>
                            <input value="<?php echo $Recaudacion->ambiente_rec ?>" type="text" class="form-control" name="ambiente_rec" id="ambiente_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="emision_rev" class="form-label">Emicion</label>
                            <input value="<?php echo $Recaudacion->emision_rev ?>" type="text" class="form-control" name="emision_rev" id="emision_rev" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="clave_acceso_rec" class="form-label">Clave acceso</label>
                            <input value="<?php echo $Recaudacion->clave_acceso_rec ?>" type="text" class="form-control" name="clave_acceso_rec" id="clave_acceso_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="email_rec" class="form-label">Email</label>
                            <input value="<?php echo $Recaudacion->email_rec ?>" type="email" class="form-control" name="email_rec" id="email_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="observacion_rec" class="form-label">Observacion</label>
                            <input value="<?php echo $Recaudacion->observacion_rec ?>" type="text" class="form-control" name="observacion_rec" id="observacion_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="nombre_rec" class="form-label">Nombre Recaudacion</label>
                            <input value="<?php echo $Recaudacion->nombre_rec ?>" type="text" class="form-control" name="nombre_rec" id="nombre_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="identificacion_rec" class="form-label">identificacion</label>
                            <input value="<?php echo $Recaudacion->identificacion_rec ?>" type="number" class="form-control" name="identificacion_rec" id="identificacion_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="direccion_rec" class="form-label">Direccion</label>
                            <input value="<?php echo $Recaudacion->direccion_rec ?>" type="text" class="form-control" name="direccion_rec" id="direccion_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="estado_rec" class="form-label">Estadoestado_rec</label>
                            <select class="form-select form-select" name="estado_rec" id="estado_rec">
                                <option value="<?php echo $Recaudacion->estado_rec ?>" selected><?php echo $Recaudacion->estado_rec ?></option>
                                <option value="ABIERTO">ABIERTO</option>
                                <option value="CERRADO">CERRADO</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="fecha_emision_rec" class="form-label">Fecha emicion</label>
                            <input value="<?php echo date('Y-m-d H:i:s'); ?>" type="datetime-local" class="form-control" name="fecha_emision_rec" id="fecha_emision_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="fecha_creacion_rec" class="form-label">Fecha Creacion</label>
                            <input value="<?php echo date('Y-m-d H:i:s'); ?>" type="datetime-local" class="form-control" name="fecha_creacion_rec" id="fecha_creacion_rec" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="fecha_actualizacion_rec" class="form-label">Fecha Creacion</label>
                            <input value="<?php echo date('Y-m-d H:i:s'); ?>" type="datetime-local" class="form-control" name="fecha_actualizacion_rec" id="fecha_actualizacion_rec" aria-describedby="helpId" placeholder="" />
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="mb-3">
                            <label for="fk_id_soc" class="form-label">Socio</label>
                            <select class="form-select form-select" name="fk_id_soc" id="fk_id_soc">
                                <option value="<?php echo $Recaudacion->fk_id_soc ?>" selected><?php echo $Recaudacion->fk_id_soc ?></option>
                                <?php foreach ($Socio as $registro) { ?>
                                    <option value="<?php echo $registro->id_soc ?>"><?php echo $registro->nombres_soc . " " . $registro->primer_apellido_soc ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="<?php echo site_url("/Recaudaciones/index"); ?>" class="btn btn-dark">Cancelar</a>

            </center>
        </div>
            </form>
        </div>
    </div>

</div>