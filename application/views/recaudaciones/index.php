<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #76D7C4; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">

        <div class="card-body">
            <div class="row">
                <div class="col-6">
                    <br>

                    <h3><b>Collections</b></h3>
                </div>
                <!-- Example single danger button -->
                <div class="col-6 text-right">
                    <br>
                    <a name="" id="" class="btn btn-primary" href="<?php echo site_url("Recaudaciones/nuevo") ?>" role="button"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-person-plus" viewBox="0 0 16 16">
                    </svg>New Collections</a>
                </div>
            </div><br>
            <?php if ($tblRecaudacion) { ?>
                <div class="table-responsive">
                  <style>
                      .celeste {
                          background-color: #ADD8E6; /* Celeste */
                      }
                  </style>

                  <table class="table table-striped table-bordered table-hover celeste" id="tblSocio">
                        <thead class="table table-info">
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Factura</th>
                                <th scope="col">Numero autorizacion</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Ambiente</th>
                                <th scope="col">Emicion</th>
                                <th scope="col">Clave de acceso</th>
                                <th scope="col">Email</th>
                                <th scope="col">Observacion</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Identificacion</th>
                                <th scope="col">Direccion</th>
                                <th scope="col">Estado</th>
                                <th scope="col">Fecha emicion</th>
                                <th scope="col">Fecha creacion</th>
                                <th scope="col">Fecha actualizacion</th>
                                <th scope="col">Socio</th>
                                <!-- <th scope="col">usuario</th> -->
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($tblRecaudacion as $registro) { ?>
                                <tr>
                                    <td><?php echo $registro->id_rec ?></td>
                                    <td><?php echo $registro->numero_factura_rec ?></td>
                                    <td><?php echo $registro->numero_autorizacion_rec ?></td>
                                    <td><?php echo $registro->fecha_hora_autorizacion_rec ?></td>
                                    <td><?php echo $registro->ambiente_rec ?></td>
                                    <td><?php echo $registro->emision_rev ?></td>
                                    <td><?php echo $registro->clave_acceso_rec ?></td>
                                    <td><?php echo $registro->email_rec ?></td>
                                    <td><?php echo $registro->observacion_rec ?></td>
                                    <td><?php echo $registro->nombre_rec ?></td>
                                    <td><?php echo $registro->identificacion_rec ?></td>
                                    <td><?php echo $registro->direccion_rec ?></td>
                                    <td><?php echo $registro->estado_rec ?></td>
                                    <td><?php echo $registro->fecha_emision_rec ?></td>
                                    <td><?php echo $registro->fecha_creacion_rec ?></td>
                                    <td><?php echo $registro->fecha_actualizacion_rec ?></td>
                                    <td><?php echo $registro->tipo_soc ?></td>

                                    <td>
                                        <a href="<?php echo site_url("/Recaudaciones/editar/$registro->id_rec") ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-fill" viewBox="0 0 16 16">
                                                <path d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                            </svg></a>&nbsp;&nbsp;&nbsp;
                                        <a href="<?php echo site_url("/Recaudaciones/eliminar/$registro->id_rec") ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16" style="color: red;" onclick="return confirm('Estas seguro de eliminar el registro')">
                                                <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z" />
                                            </svg></a>
                                    </td>
                                </tr>
                            <?php } ?>

                        </tbody>
                    </table>
                </div>
            <?php } ?>

        </div>
    </div>

</div>


<script type="text/javascript">
    $("#tblSocio")
        .DataTable({
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
        });
</script>
