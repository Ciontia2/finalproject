<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container">
    <div class="card">
    <center>
    <h3>
            <center>
                <br><b>
                    EDIT SETTING
                </b>

            </center>
            </h3>
        <div class="card-body">

    <form action="<?php echo site_url("/configuracions/procesarActualizacion") ?>" method="post">
        <div class="row">

        <input value="<?php echo $configuracionEditar->id_con ?>" hidden type="text" class="form-control" name="id_con" id="id_con" aria-describedby="helpId" placeholder="" />

        <div class="col-md-4">
              <label for="">NAME: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="text" class="form-control" required name="nombre_con" value="<?php echo $configuracionEditar->nombre_con ?>" id="nombre_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">R.U.C: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="number" class="form-control" required name="ruc_con" value="<?php echo $configuracionEditar->ruc_con ?>" id="ruc_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">LOGO: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="text" class="form-control" required name="logo_con" value="<?php echo $configuracionEditar->logo_con ?>" id="logo_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">PHONE: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="number" class="form-control" required name="telefono_con" value="<?php echo $configuracionEditar->telefono_con ?>" id="telefono_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">ADDRESS: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="text" class="form-control" required name="direccion_con" value="<?php echo $configuracionEditar->direccion_con ?>" id="direccion_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">E-MAIL: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="email" class="form-control" required name="email_con" value="<?php echo $configuracionEditar->email_con ?>" id="email_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">SERVER: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="text" class="form-control" required name="servidor_con" value="<?php echo $configuracionEditar->servidor_con ?>" id="servidor_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">PORT: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="number" class="form-control" required name="puerto_con" value="<?php echo $configuracionEditar->puerto_con ?>" id="puerto_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">PASSWORD: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="text" class="form-control" required name="password_con" value="<?php echo $configuracionEditar->password_con ?>" id="password_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">CREATION: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="datetime" class="form-control" required name="creacion_con" value="<?php echo $configuracionEditar->creacion_con ?>" id="creacion_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">UPDATE: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="datetime" class="form-control" required name="actualizacion_con" value="<?php echo $configuracionEditar->actualizacion_con ?>" id="actualizacion_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">INITIAL YEAR: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="number" class="form-control" required name="anio_inicial_con" value="<?php echo $configuracionEditar->anio_inicial_con ?>" id="anio_inicial_con" style="background-color: white;">
          </div>
          <div class="col-md-4">
              <label for="">INITIAL MONTH: <span class="obligatorio">(Obligatorio)</span></label>
              <br>
              <input type="text" class="form-control" required name="mes_inicial_con" value="<?php echo $configuracionEditar->mes_inicial_con ?>" id="mes_inicial_con" style="background-color: white;">
          </div>
        <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-primary">keep</button>
                <a href="<?php echo site_url(); ?>/configuracions/index" class="btn btn-dark">Cancelar</a>

            </center>
        </div>
    </form>
        </div>
    </div>

</div>
