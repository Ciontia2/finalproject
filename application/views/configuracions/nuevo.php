<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
<h4>
            <center>
                <br><b>
                    New Setting
                </b>

            </center>
            </h4>
    <div class="card">

        <div class="card-body">
        <form action="<?php echo site_url("/configuracions/guardar") ?>" method="post">
        <div class="row">
          <div class="col-md-4">
                  <label for="">NAME: <span </span></label>
                  <br>
                  <input type="text" class="form-control" required name="nombre_con" value="" id="nombre_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">R.U.C: <span </span></label>
                  <br>
                  <input type="number" class="form-control" required name="ruc_con" value="" id="ruc_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">LOGO: <span </span></label>
                  <br>
                  <input type="text" class="form-control" required name="logo_con" value="" id="logo_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">PHONE: <span </span></label>
                  <br>
                  <input type="number" class="form-control" required name="telefono_con" value="" id="telefono_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">ADDRESS: <span </span></label>
                  <br>
                  <input type="text" class="form-control" required name="direccion_con" value="" id="direccion_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">E-MAIL: <span </span></label>
                  <br>
                  <input type="email" class="form-control" required name="email_con" value="" id="email_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">SERVER: <span </span></label>
                  <br>
                  <input type="text" class="form-control" required name="servidor_con" value="" id="servidor_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">PORT: <span </span></label>
                  <br>
                  <input type="number" class="form-control" required name="puerto_con" value="" id="puerto_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">PASSWORD: <span </span></label>
                  <br>
                  <input type="text" class="form-control" required name="password_con" value="" id="password_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">CREATION: <span </span></label>
                  <br>
                  <input type="datetime-local" class="form-control" required name="creacion_con" value="" id="creacion_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">UPDATE: <span </span></label>
                  <br>
                  <input type="datetime-local" class="form-control" required name="actualizacion_con" value="" id="actualizacion_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                <label for="">INITIAL YEAR: <span </span></label>
                  <br>
                  <input type="number" class="form-control" required name="anio_inicial_con" value="" id="anio_inicial_con" style="background-color: white;"required>
              </div>
              <div class="col-md-4">
                  <label for="">INITIAL MONTH: <span </span></label>
                  <br>
                  <input type="text" class="form-control" required name="mes_inicial_con" value="" id="mes_inicial_con" style="background-color: white;"required>
              </div>
        </div>

        <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url(); ?>/configuracions/index" class="btn btn-danger">Cancel</a>

            </center>
        </div>
    </form>
        </div>
    </div>

</div>
</div></div>
<script type="text/javascript">
  $(document).ready(function () {
    // DataTable initialization
    $("#tablaconfiguracion").DataTable();

    // Form validation
    $("#yourFormId").validate({
      rules: {
        nombre_con: {
          required: true,
          minlength: 2,
          maxlength: 50
        },
        ruc_con: {
          required: true,
          digits: true,
          minlength: 11,
          maxlength: 11
        },
        telefono_con: {
          required: true,
          digits: true,
          minlength: 7,
          maxlength: 15
        },
        direccion_con: {
          required: true,
          minlength: 5,
          maxlength: 100
        },
        email_con: {
          required: true,
          email: true
        },
        servidor_con: {
          required: true,
          minlength: 5,
          maxlength: 50
        },
        puerto_con: {
          required: true,
          digits: true
        },
        password_con: {
          required: true,
          minlength: 8,
          maxlength: 20
        },
        anio_inicial_con: {
          required: true,
          digits: true,
          minlength: 4,
          maxlength: 4
        },
        mes_inicial_con: {
          required: true,
          digits: true,
          min: 1,
          max: 12
        },
        // Add more rules for other fields
      },
      messages: {
        nombre_con: {
          required: "Please enter a name",
          minlength: "Name must be at least 2 characters",
          maxlength: "Name must not exceed 50 characters"
        },
        ruc_con: {
          required: "Please enter a valid R.U.C",
          digits: "R.U.C must consist of digits only",
          minlength: "R.U.C must be 11 digits",
          maxlength: "R.U.C must be 11 digits"
        },
        telefono_con: {
          required: "Please enter a valid phone number",
          digits: "Phone number must consist of digits only",
          minlength: "Phone number must be at least 7 digits",
          maxlength: "Phone number must not exceed 15 digits"
        },
        direccion_con: {
          required: "Please enter an address",
          minlength: "Address must be at least 5 characters",
          maxlength: "Address must not exceed 100 characters"
        },
        email_con: {
          required: "Please enter a valid email address",
          email: "Please enter a valid email address"
        },
        servidor_con: {
          required: "Please enter a server name",
          minlength: "Server name must be at least 5 characters",
          maxlength: "Server name must not exceed 50 characters"
        },
        puerto_con: {
          required: "Please enter a valid port",
          digits: "Port must consist of digits only"
        },
        password_con: {
          required: "Please enter a password",
          minlength: "Password must be at least 8 characters",
          maxlength: "Password must not exceed 20 characters"
        },
        anio_inicial_con: {
          required: "Please enter the initial year",
          digits: "Year must consist of digits only",
          minlength: "Year must be 4 digits",
          maxlength: "Year must be 4 digits"
        },
        mes_inicial_con: {
          required: "Please enter the initial month",
          digits: "Month must consist of digits only",
          min: "Month must be between 1 and 12",
          max: "Month must be between 1 and 12"
        },
        // Add more messages for other fields
      },
      errorPlacement: function (error, element) {
        error.insertAfter(element);
      }
    });
  });
</script>
