<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
    <center>
        <h1>NEW ASSISTS</h1>
    </center>
    <form class="" id="frm_nuevo_asistencia" action="<?php echo site_url("asistencias/guardar"); ?>" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <label for="">GUY: <span class="obligatorio"></span></label>
                    <br>
                    <select class="form-control" required name="tipo_asi" id="tipo_asi" style="background-color: white;">
                        <option value="SOCIO">SOCIO</option>
                        <option value="FALTA">FALTA</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">WORTH: <span class="obligatorio"></span></label>
                    <br>
                    <input type="number" class="form-control" required name="valor_asi" value="" id="valor_asi" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">BACKWARDNESS: <span class="obligatorio"></span></label>
                    <br>
                    <select class="form-control" required name="atraso_asi" id="atraso_asi" style="background-color: white;">
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">DELAY VALUE: <span class="obligatorio"></span></label>
                    <br>
                    <input type="number" class="form-control" required name="valor_atraso_asi" value="" id="valor_atraso_asi" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CREATION: <span class="obligatorio"></span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="creacion_asi" value="" id="creacion_asi" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">UPDATE: <span class="obligatorio"></span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="actualizacion_asi" value="" id="actualizacion_asi" style="background-color: white;">
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_soc" class="form-label">Socio</label>
                            <select class="form-select form-select" name="fk_id_soc" id="fk_id_soc">
                                <option selected>Select one</option>
                                <?php foreach($Socio as $registro){ ?>
                                <option value="<?php echo $registro->id_soc ?>"><?php echo $registro->nombres_soc ." ".$registro->primer_apellido_soc ?></option>
                              <?php } ?>
                            </select>
                        </div>

                    </div>
                    <div class="col-6">
                    <div class="mb-3">
                            <label for="fk_id_eve" class="form-label">Socio</label>
                            <select class="form-select form-select" name="fk_id_eve" id="fk_id_eve">
                                <option selected>Select one</option>
                                <?php foreach($Evento as $registro){ ?>
                                <option value="<?php echo $registro->id_eve ?>"><?php echo $registro->descripcion_eve." ".$registro->fecha_hora_eve ?></option>
                              <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-success">
                    keep
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/asistencias/index" class="btn btn-danger">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div></div></div>
