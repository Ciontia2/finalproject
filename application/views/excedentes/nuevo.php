<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
        <h4>
            <center>
                <br><b>
                    new Ecxedente
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("/Excedentes/guardar") ?>" method="post">
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="limite_minimo_ex" class="form-label">Limite minimo</label>
                            <input type="number" class="form-control" name="limite_minimo_ex" id="limite_minimo_ex" aria-describedby="helpId" placeholder="" />
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="limite_maximo_ex" class="form-label">Limite maximo</label>
                            <input type="text" class="form-control" name="limite_maximo_ex" id="limite_maximo_ex" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="tarifa_ex" class="form-label">tarifa</label>
                            <input step="any" type="number" class="form-control" name="tarifa_ex" id="tarifa_ex" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="fecha_actualizacion_ex" class="form-label">Fecha Actualizacion</label>
                            <input value="<?php echo date('Y-m-d H:i:s'); ?>"  type="datetime-local" class="form-control" name="fecha_actualizacion_ex" id="fecha_actualizacion_ex" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="fecha_creacion_ex" class="form-label">Fecha Creacion</label>
                            <input value="<?php echo date('Y-m-d H:i:s'); ?>" type="datetime-local" class="form-control" name="fecha_creacion_ex" id="fecha_creacion_ex" aria-describedby="helpId" placeholder="" />
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mb-3">
                            <label for="id_tar" class="form-label">Tarifa fk</label>
                            <select class="form-select form-select" name="id_tar" id="id_tar">
                                <option selected>Select one</option>
                                <?php foreach($tarifa as $registro){ ?>
                                <option value="<?php echo $registro->id_tar ?>"><?php echo $registro->nombre_tar ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url("/Excedentes/index"); ?>" class="btn btn-danger">Cancel</a>

            </center>
        </div>


            </form>
        </div>
    </div>
  </div>
</div>
