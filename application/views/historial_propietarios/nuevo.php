<?php
date_default_timezone_set('America/Guayaquil');

?>
<br><br>
<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
    <h4>
        <center>
            <br><b>
                New Owner History
            </b>

        </center>
    </h4>
    <form class="" id="frm_nuevo_historial_propietario" action="<?php echo site_url("historial_propietarios/guardar"); ?>" method="post">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <label for="">UPDATE: <span historial_propietario</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="actualizacion_his" value="" id="actualizacion_his" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">SATTE: <span historial_propietario</span></label>
                    <br>
                    <select class="form-control" required name="estado_his" value="" id="estado_his" style="background-color: white;">
                        <option value="ACTIVO">ACTIVO</option>
                        <option value="INACTIVO">INACTIVO</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">OBSERVATION: <span historial_propietario</span></label>
                    <br>
                    <select class="form-control" required name="observacion_his" value="" id="observacion_his" style="background-color: white;">
                        <option value="NUEVO">NUEVO</option>
                        <option value="ANTIGUO">ANTIGUO</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="">DATE CHANGE: <span historial_propietario</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="fecha_cambio_his" value="" id="fecha_cambio_his" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CREATION: <span historial_propietario</span></label>
                    <br>
                    <input type="datetime-local" class="form-control" required name="creacion_his" value="" id="creacion_his" style="background-color: white;">
                </div>
                <div class="col-md-4">
                    <label for="">CURRENT OWNER: <span historial_propietario</span></label>
                    <br>
                    <select class="form-control" required name="propietario_actual_his" value="" id="propietario_actual_his" style="background-color: white;">
                        <option value="SI">SI</option>
                        <option value="NO">NO</option>
                    </select>
                </div>

            </div>
            <div class="row">
                <div class="col-6">
                    <div class="mb-3">
                        <label for="fk_id_soc" class="form-label">Socio</label>
                        <select class="form-select form-select" name="fk_id_soc" id="fk_id_soc">
                            <option selected>Select one</option>
                            <?php foreach ($socio as $registro) { ?>
                                <option value="<?php echo $registro->id_soc  ?>"><?php echo $registro->nombres_soc . " " . $registro->primer_apellido_soc ?></option>
                            <?php } ?>
                        </select>
                    </div>

                </div>
                <div class="col-6">
                    <div class="mb-3">
                        <label for="fk_id_med" class="form-label">meditor</label>
                        <select class="form-select form-select" name="fk_id_med" id="fk_id_med">
                            <option selected>Select one</option>
                            <?php foreach ($meditor as $registro) { ?>
                                <option value="<?php echo $registro->id_med ?><"><?php echo $registro->numero_med ?></option>
                            <?php } ?>
                        </select>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-success">
                    keep
                </button>
                &nbsp;&nbsp;&nbsp;
                <a href="<?php echo site_url(); ?>/historial_propietarios/index" class="btn btn-danger">
                    Cancel
                </a>
            </div>
        </div>
    </form>
</div></div></div>
