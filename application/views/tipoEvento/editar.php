<?php
date_default_timezone_set('America/Guayaquil');

?>


<div class="container">
    <div class="card">
        <h4>
            <center>
                <br><b>
                    Edit Tipo Event
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("TipoEventos/actualizar") ?>" method="post">
            <input hidden value="<?php echo $Tipoevento->id_te ?>" type="text" class="form-control" name="id_te" id="id_te" aria-describedby="helpId" placeholder="" />

            <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="nombre_te" class="form-label">Nombre</label>
                            <input value="<?php echo $Tipoevento->nombre_te ?>" type="text" class="form-control" name="nombre_te" id="nombre_te" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="estado_te" class="form-label">Estado</label>
                            <select class="form-select form-select" name="estado_te" id="estado_te">
                                <option value="<?php echo $Tipoevento->estado_te ?>" selected><?php echo $Tipoevento->estado_te ?></option>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="creacion_te" class="form-label">Cracion</label>
                            <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="creacion_te" id="creacion_te" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="actualizacion_te" class="form-label">Actualizacion</label>
                            <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="actualizacion_te" id="actualizacion_te" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-primary">keep</button>
                <a href="<?php echo site_url("/TipoEventos/index"); ?>" class="btn btn-dark">Cancelar</a>

            </center>
        </div>
            </form>
        </div>
    </div>

</div>