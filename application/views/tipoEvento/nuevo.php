<?php
date_default_timezone_set('America/Guayaquil');

?>


<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
        <h4>
            <center>
                <br><b>
                    New Tipo Event
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("TipoEventos/guardar") ?>" method="post">
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="nombre_te" class="form-label">Name</label>
                            <input type="text" class="form-control" name="nombre_te" id="nombre_te" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="estado_te" class="form-label">Status</label>
                            <select class="form-select form-select" name="estado_te" id="estado_te">
                                <option selected>Select one</option>
                                <option value="ACTIVO">ACTIVO</option>
                                <option value="INACTIVO">INACTIVO</option>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="creacion_te" class="form-label">Creations</label>
                            <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="creacion_te" id="creacion_te" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="actualizacion_te" class="form-label">Update</label>
                            <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="actualizacion_te" id="actualizacion_te" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
            <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url("/TipoEventos/index"); ?>" class="btn btn-danger">Cancel</a>


            </center>
        </div>
            </form>
        </div>
    </div>

</div>







        </div>
