<?php
date_default_timezone_set('America/Guayaquil');

?>

<div class="container-fluid">
  <style>
    .custom-card {
        background-color: #D6EAF8; /* Azul claro */
        border: 1px solid #B0C4DE; /* Borde de color más oscuro */
    }
</style>
    <div class="card custom-card">
        <div class="card-body">
        <h4>
            <center>
                <br><b>
                    new Events
                </b>

            </center>
        </h4>
        <div class="card-body">
            <form action="<?php echo site_url("Eventos/guardar") ?>" method="post">
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="descripcion_eve" class="form-label">Descripcion</label>
                            <input type="text" class="form-control" name="descripcion_eve" id="descripcion_eve" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fecha_hora_eve" class="form-label">Fecha</label>
                            <input value="<?php echo date('Y-m-d'); ?>" type="date" class="form-control" name="fecha_hora_eve" id="fecha_hora_eve" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="lugar_eve" class="form-label">Lugar</label>
                            <input type="text" class="form-control" name="lugar_eve" id="lugar_eve" aria-describedby="helpId" placeholder="" />
                        </div>

                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="fk_id_te" class="form-label">Tipo Evento</label>
                            <select class="form-select form-select" name="fk_id_te" id="fk_id_te">
                                <option selected>Select one</option>
                                <?php foreach($eventos as $reg){ ?>
                                <option value="<?php echo $reg->id_te ?>"><?php echo $reg->nombre_te ?></option>
                                <?php } ?>
                            </select>
                        </div>

                    </div>
                </div>
                <div class="row">
                <center>
                <br>
                <button type="submit" class="btn btn-success">keep</button>
                <a href="<?php echo site_url("/Eventos/index"); ?>" class="btn btn-danger">Cancel</a>

            </center>
                </div>
            </form>
        </div>
    </div>

</div>
</div>
