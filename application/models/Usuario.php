<?php
 class Usuario extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("usuario",$datos);
    }
    //funcion para consultar ucusuarios
    function obtenerTodos(){
        $this->db->join("perfil","usuario.fk_id_per = perfil.id_per");
        $listadousuarios=$this->db->get("usuario");
        if($listadousuarios->num_rows()>0)//si hay datos
        {
            return $listadousuarios->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_usu){
        $this->db->where("id_usu",$id_usu);
        if($this->db->delete("usuario")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_usu){
            $this->db->join("perfil","usuario.fk_id_per = perfil.id_per");
            $this->db->where("id_usu",$id_usu);
            $usuario=$this->db->get("usuario");
            if($usuario->num_rows()>0){
                return $usuario->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_usu,$datos){
        $this->db->where("id_usu",$id_usu);
        return $this->db->update ('usuario',$datos);
        }

 } // Cierre de la clase
 ?>
