<?php
 class Ruta extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("ruta",$datos);
    }
    //funcion para consultar ucrutas
    function obtenerTodos(){
        // $this->db->join("usuario","usuario.id_usu = ruta.id_rut");
        $listadorutas=$this->db->get("ruta");
        if($listadorutas->num_rows()>0)//si hay datos
        {
            return $listadorutas->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_rut){
        $this->db->where("id_rut",$id_rut);
        if($this->db->delete("ruta")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_rut){
            $this->db->where("id_rut",$id_rut);
            $ruta=$this->db->get("ruta");
            if($ruta->num_rows()>0){
                return $ruta->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_rut,$datos){
        $this->db->where("id_rut",$id_rut);
        return $this->db->update ('ruta',$datos);
        }

 } // Cierre de la clase
 ?>
