<?php
 class Tarifa extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("tarifa",$datos);
    }
    //funcion para consultar uctarifas
    function obtenerTodos(){
        // $this->db->join("usuario","usuario.id_usu = tarifa.id_tar");
        $listadotarifas=$this->db->get("tarifa");
        if($listadotarifas->num_rows()>0)//si hay datos
        {
            return $listadotarifas->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_tar){
        $this->db->where("id_tar",$id_tar);
        if($this->db->delete("tarifa")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_tar){
            $this->db->where("id_tar",$id_tar);
            $tarifa=$this->db->get("tarifa");
            if($tarifa->num_rows()>0){
                return $tarifa->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_tar,$datos){
        $this->db->where("id_tar",$id_tar);
        return $this->db->update ('tarifa',$datos);
        }

 } // Cierre de la clase
 ?>
