<?php
 class Socio extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("socio",$datos);
    }
    //funcion para consultar ucsocios
    function obtenerTodos(){
        // $this->db->join("usuario","usuario.id_usu = socio.id_soc");
        $listadosocios=$this->db->get("socio");
        if($listadosocios->num_rows()>0)//si hay datos
        {
            return $listadosocios->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_soc){
        $this->db->where("id_soc",$id_soc);
        if($this->db->delete("socio")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_soc){
            $this->db->where("id_soc",$id_soc);
            $socio=$this->db->get("socio");
            if($socio->num_rows()>0){
                return $socio->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_soc,$datos){
        $this->db->where("id_soc",$id_soc);
        return $this->db->update ('socio',$datos);
        }

 } // Cierre de la clase
 ?>
