<?php
 class Evento extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("evento",$datos);
    }
    //funcion para consultar uceventos
    function obtenerTodos(){
        $this->db->join("tipo_evento","tipo_evento.id_te = evento.id_eve");
        $listadoEventos=$this->db->get("evento");
        if($listadoEventos->num_rows()>0)//si hay datos
        {
            return $listadoEventos->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_eve){
        $this->db->where("id_eve",$id_eve);
        if($this->db->delete("evento")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_eve){
            $this->db->where("id_eve",$id_eve);
            $evento=$this->db->get("evento");
            if($evento->num_rows()>0){
                return $evento->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_eve,$datos){
        $this->db->where("id_eve",$id_eve);
        return $this->db->update ('evento',$datos);
        }

 } // Cierre de la clase
 ?>
