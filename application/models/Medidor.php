<?php
 class Medidor extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("medidor",$datos);
    }
    //funcion para consultar ucemedidors
    function obtenerTodos(){
        $listadoMedidors=$this->db->get("medidor");
        if($listadoMedidors->num_rows()>0)//si hay datos
        {
            return $listadoMedidors->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_med){
        $this->db->where("id_med",$id_med);
        if($this->db->delete("medidor")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_med){
            $this->db->where("id_med",$id_med);
            $medidor=$this->db->get("medidor");
            if($medidor->num_rows()>0){
                return $medidor->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_med,$datos){
        $this->db->where("id_med",$id_med);
        return $this->db->update ('medidor',$datos);
        }

 } // Cierre de la clase
 ?>
