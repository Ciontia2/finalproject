<?php
 class Perfil extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("perfil",$datos);
    }
    //funcion para consultar ucperfils
    function obtenerTodos(){
        $listadoperfils=$this->db->get("perfil");
        if($listadoperfils->num_rows()>0)//si hay datos
        {
            return $listadoperfils->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_per){
        $this->db->where("id_per",$id_per);
        if($this->db->delete("perfil")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_per){
            $this->db->where("id_per",$id_per);
            $perfil=$this->db->get("perfil");
            if($perfil->num_rows()>0){
                return $perfil->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_per,$datos){
        $this->db->where("id_per",$id_per);
        return $this->db->update ('perfil',$datos);
        }

 } // Cierre de la clase
 ?>
