<?php
 class Asistencia extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("asistencia",$datos);
    }
    //funcion para consultar uciasistencias
    function obtenerTodos(){
        $listadoAsistencias=$this->db->get("asistencia");
        if($listadoAsistencias->num_rows()>0)//si hay datos
        {
            return $listadoAsistencias->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_asi){
        $this->db->where("id_asi",$id_asi);
        if($this->db->delete("asistencia")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_asi){
            $this->db->where("id_asi",$id_asi);
            $asistencia=$this->db->get("asistencia");
            if($asistencia->num_rows()>0){
                return $asistencia->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_asi,$datos){
        $this->db->where("id_asi",$id_asi);
        return $this->db->update ('asistencia',$datos);
        }

 } // Cierre de la clase
 ?>

