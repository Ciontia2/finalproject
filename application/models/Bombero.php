<?php
class Bombero extends CI_Model {

  public function __construct() {
      parent::__construct();
  }



  function getAll()
  {
    $listMovie=
    $this->db->get("contador");
    if($listMovie->num_rows()>0){
        return $listMovie->result();
    }else{
        return false;
    }
  }
  // *********************** ACTIVIDAD 3 ****************************
  // FUNCION 1: actividad 3
  function getByVisit20()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2020
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }


  function getByVisit21()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2021
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }

  function getByVisit22()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2022
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }

  function getByVisit23()
  {
    $sql="SELECT
      CASE
      WHEN MONTH(contador.fecha_con) = 1 THEN 'January'
      WHEN MONTH(contador.fecha_con) = 2 THEN 'February'
      WHEN MONTH(contador.fecha_con) = 3 THEN 'March'
      WHEN MONTH(contador.fecha_con) = 4 THEN 'April'
      WHEN MONTH(contador.fecha_con) = 5 THEN 'May'
      WHEN MONTH(contador.fecha_con) = 6 THEN 'June'
      WHEN MONTH(contador.fecha_con) = 7 THEN 'July'
      WHEN MONTH(contador.fecha_con) = 8 THEN 'August'
      WHEN MONTH(contador.fecha_con) = 9 THEN 'September'
      WHEN MONTH(contador.fecha_con) = 10 THEN 'October'
      WHEN MONTH(contador.fecha_con) = 11 THEN 'November'
      WHEN MONTH(contador.fecha_con) = 12 THEN 'December'
      END as Months,
      COUNT(codigo_con) as Total
    FROM
      contador
    WHERE
      YEAR(contador.fecha_con) = 2023
    GROUP BY
      Months
    ORDER BY
      MONTH(contador.fecha_con);";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->result();
    } else {
      return 0;
    }
  }
  //INDICADOR 1: actividad 3
  function getByTotalVisit20()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2020;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 2: actividad 3
  function getByTotalVisit21()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2021;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 3: actividad 3
  function getByTotalVisit22()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2022;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }

  //INDICADOR 4: actividad 3
  function getByTotalVisit23()
  {
    $sql="sELECT COUNT(codigo_con) as total_visits FROM contador WHERE YEAR(contador.fecha_con) = 2023;";
    $result=$this->db->query($sql);
    if ($result->num_rows()>0) {
      return $result->row()->total_visits;
    } else {
      return 0;
    }
  }
  // *********************** FIN ACTIVIDAD 3 ****************************


  // *********************** ACTIVIDAD 4 ********************************
  //LIZ

  public function getTotalNotificaciones()
  {
      $sql = "SELECT SUM(total_notificaciones) AS suma_total FROM (SELECT COUNT(n.codigo_not) AS total_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.codigo_sol) AS subquery;";
      $result = $this->db->query($sql);

      if ($result->num_rows() > 0) {
          $row = $result->row();
          return $row->suma_total;
      } else {
          return 0;
      }
  }
  public function getTotalNotificacionesTop10()
  {
      $sql = "SELECT SUM(total_notificaciones) AS suma_total FROM (SELECT COUNT(n.codigo_not) AS total_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.codigo_sol ORDER BY total_notificaciones DESC LIMIT 10) AS subquery;";
      $result = $this->db->query($sql);

      if ($result->num_rows() > 0) {
          $row = $result->row();
          return $row->suma_total;
      } else {
          return 0;
      }
  }


  function getByTotalNotification($order,$limit){
        $sql="select s.codigo_sol, COUNT(n.codigo_not) AS total_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.codigo_sol ORDER BY total_notificaciones $order LIMIT $limit  ;";
        $result=$this->db->query($sql);
        if($result->num_rows()>0){
            return $result->result();
          }else {
            return 0;
          }
        }


        public function getTotalEstados()
       {
           $sql = "select COUNT(DISTINCT estado_sol) AS total_estados FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol;";
           $result = $this->db->query($sql);

           if ($result->num_rows() > 0) {
               $row = $result->row();
               return $row->total_estados;
           } else {
               return 0;
           }
       }
        function getByTotalState($order){
              $sql="select estado_sol, COUNT(n.codigo_not) AS count_notificaciones, (COUNT(n.codigo_not) / (SELECT COUNT(codigo_not) FROM notificacion)) * 100 AS porcentaje FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY estado_sol $order ;";
              $result=$this->db->query($sql);
              if($result->num_rows()>0){
                  return $result->result();
                }else {
                  return 0;
                }
              }

              public function getTotalActividades()
              {
                  $sql = "select COUNT(n.codigo_not) AS total_notificaciones
                    FROM solicitud_permiso s
                    LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol;";
                  $result = $this->db->query($sql);

                  if ($result->num_rows() > 0) {
                      $row = $result->row();
                      return $row->total_notificaciones;
                  } else {
                      return 0;
                  }
              }
              function getByTotalActividad($order,$limit){
                    $sql="select s.actividad_sol, YEAR(n.fecha_not) AS anio, COUNT(n.codigo_not) AS count_notificaciones FROM solicitud_permiso s LEFT JOIN notificacion n ON s.codigo_sol = n.codigo_sol GROUP BY s.actividad_sol, anio ORDER BY count_notificaciones $order LIMIT $limit ;";
                    $result=$this->db->query($sql);
                    if($result->num_rows()>0){
                        return $result->result();
                      }else {
                        return 0;
                      }
                    }

                                function getTotalSolicitudes() {
                    $sql = "select COUNT(codigo_sol) AS total_solicitudes FROM solicitud_permiso;";
                    $result = $this->db->query($sql);

                    if ($result->num_rows() > 0) {
                        $row = $result->row();
                        return $row->total_solicitudes;
                    } else {
                        return 0;
                    }
                }

                    function getByTotalMes($order){
                          $sql="select anio, COUNT(codigo_sol) AS count_solicitudes, (COUNT(codigo_sol) * 100.0 / SUM(COUNT(codigo_sol)) OVER ()) AS porcentaje FROM ( SELECT YEAR(fecha_sol) AS anio, codigo_sol FROM solicitud_permiso ) AS subquery GROUP BY anio ORDER BY anio $order; ";
                          $result=$this->db->query($sql);
                          if($result->num_rows()>0){
                              return $result->result();
                            }else {
                              return 0;
                            }
                          }
  // *********************** FIN ACTIVIDAD 4 ****************************


  // *********************** ACTIVIDAD 5 ********************************
  function getCantidadMensajesPorAnio() {
        $sql = "SELECT l.anio_lot, COUNT(m.codigo_men) AS cantidad_mensajes
                FROM lotaip l
                LEFT JOIN mensaje m ON YEAR(m.fecha_men) = l.anio_lot
                GROUP BY l.anio_lot;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getTotalMensajes() {
    $sql = "SELECT COUNT(codigo_men) AS total_mensajes FROM mensaje";

    $result = $this->db->query($sql);

    if ($result->num_rows() > 0) {
        $total = $result->row()->total_mensajes;
        return $total;
    } else {
        return 0;
    }
}

    function getMensajesPorAnioYMes() {
        $sql = "SELECT YEAR(m.fecha_men) AS anio, MONTH(m.fecha_men) AS mes, COUNT(m.codigo_men) AS cantidad_mensajes
                FROM mensaje m
                GROUP BY YEAR(m.fecha_men), MONTH(m.fecha_men)
                ORDER BY anio, mes;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getCantidadArchivosPorTipo() {
        $sql = "SELECT 'Archivo' AS tipo, COUNT(*) AS cantidad_archivos
                FROM archivo
                UNION ALL
                SELECT 'Galeria' AS tipo, COUNT(*) AS cantidad_galerias
                FROM galeria;";

        $result = $this->db->query($sql);

        if ($result->num_rows() > 0) {
            return $result->result();
        } else {
            return 0;
        }
    }

    function getTotalArchivosYGaleriasSumados() {
    $sql = "SELECT COUNT(*) AS total FROM archivo
            UNION ALL
            SELECT COUNT(*) FROM galeria";

    $result = $this->db->query($sql);

    if ($result->num_rows() > 0) {
        $rows = $result->result_array();
        $total = 0;

        foreach ($rows as $row) {
            $total += $row['total'];
        }

        return $total;
    } else {
        return 0;
    }
}


  // *********************** FIN ACTIVIDAD 5 ****************************


  // *********************** ACTIVIDAD 6 ********************************
  //angel
  // *********************** FIN ACTIVIDAD 6 ****************************


}//The class end
