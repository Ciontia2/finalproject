<?php
 class Historial_propietario extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("historial_propietario",$datos);
    }
    //funcion para consultar ucistorial_propietarios
    function obtenerTodos(){
        $listadoHistorial_propietarios=$this->db->get("historial_propietario");
        if($listadoHistorial_propietarios->num_rows()>0)//si hay datos
        {
            return $listadoHistorial_propietarios->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_his){
        $this->db->where("id_his",$id_his);
        if($this->db->delete("historial_propietario")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_his){
            $this->db->where("id_his",$id_his);
            $historial_propietario=$this->db->get("historial_propietario");
            if($historial_propietario->num_rows()>0){
                return $historial_propietario->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_his,$datos){
        $this->db->where("id_his",$id_his);
        return $this->db->update ('historial_propietario',$datos);
        }

 } // Cierre de la clase
 ?>
