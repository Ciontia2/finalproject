<?php
 class Impuesto extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("impuesto",$datos);
    }
    //funcion para consultar ucimpuestos
    function obtenerTodos(){
        $listadoImpuestos=$this->db->get("impuesto");
        if($listadoImpuestos->num_rows()>0)//si hay datos
        {
            return $listadoImpuestos->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_imp){
        $this->db->where("id_imp",$id_imp);
        if($this->db->delete("impuesto")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_imp){
            $this->db->where("id_imp",$id_imp);
            $impuesto=$this->db->get("impuesto");
            if($impuesto->num_rows()>0){
                return $impuesto->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_imp,$datos){
        $this->db->where("id_imp",$id_imp);
        return $this->db->update ('impuesto',$datos);
        }

 } // Cierre de la clase
 ?>
