<?php
 class Excedente extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("excedente",$datos);
    }
    //funcion para consultar ucexcedentes
    function obtenerTodos(){
        $listadoExcedentes=$this->db->get("excedente");
        if($listadoExcedentes->num_rows()>0)//si hay datos
        {
            return $listadoExcedentes->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_ex){
        $this->db->where("id_ex",$id_ex);
        if($this->db->delete("excedente")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_ex){
            $this->db->where("id_ex",$id_ex);
            $excedente=$this->db->get("excedente");
            if($excedente->num_rows()>0){
                return $excedente->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_ex,$datos){
        $this->db->where("id_ex",$id_ex);
        return $this->db->update ('excedente',$datos);
        }

 } // Cierre de la clase
 ?>

