<?php
 class Lectura extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("lectura",$datos);
    }
    //funcion para consultar uceecturas
    function obtenerTodos(){
        $listadoLecturas=$this->db->get("lectura");
        if($listadoLecturas->num_rows()>0)//si hay datos
        {
            return $listadoLecturas->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_lec){
        $this->db->where("id_lec",$id_lec);
        if($this->db->delete("lectura")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_lec){
            $this->db->where("id_lec",$id_lec);
            $lectura=$this->db->get("lectura");
            if($lectura->num_rows()>0){
                return $lectura->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_lec,$datos){
        $this->db->where("id_lec",$id_lec);
        return $this->db->update ('lectura',$datos);
        }

 } // Cierre de la clase
 ?>
