<?php
class Detalle extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // Función para insertar un detalle
    function insertar($datos)
    {
        return $this->db->insert("detalle", $datos);
    }

    // Función para consultar todos los detalles
    function obtenerTodos()
    {
        $listadoDetalles = $this->db->get("detalle");
        if ($listadoDetalles->num_rows() > 0) {
            return $listadoDetalles->result();
        } else {
            return false;
        }
    }

    // Función para borrar un detalle
    function borrar($id_det)
    {
        $this->db->where("id_det", $id_det);
        if ($this->db->delete("detalle")) {
            return true;
        } else {
            return false;
        }
    }

    // Función para obtener un detalle por su ID
    function obtenerPorId($id_det)
    {
        $this->db->where("id_det", $id_det);
        $detalle = $this->db->get("detalle");
        if ($detalle->num_rows() > 0) {
            return $detalle->row();
        }
        return false;
    }

    // Función para actualizar un detalle
    function actualizar($id_det, $datos)
    {
        $this->db->where("id_det", $id_det);
        return $this->db->update('detalle', $datos);
    }
}
?>
