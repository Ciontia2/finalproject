<?php
 class Comunicado extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("comunicado",$datos);
    }
    //funcion para consultar ucomunicados
    function obtenerTodos(){
        $listadoComunicados=$this->db->get("comunicado");
        if($listadoComunicados->num_rows()>0)//si hay datos
        {
            return $listadoComunicados->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_com){
        $this->db->where("id_com",$id_com);
        if($this->db->delete("comunicado")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_com){
            $this->db->where("id_com",$id_com);
            $comunicado=$this->db->get("comunicado");
            if($comunicado->num_rows()>0){
                return $comunicado->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_com,$datos){
        $this->db->where("id_com",$id_com);
        return $this->db->update ('comunicado',$datos);
        }

 } // Cierre de la clase
 ?>
