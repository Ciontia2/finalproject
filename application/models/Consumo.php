<?php
 class Consumo extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("consumo",$datos);
    }
    //funcion para consultar uconsumos
    function obtenerTodos(){
        $listadoConsumos=$this->db->get("consumo");
        if($listadoConsumos->num_rows()>0)//si hay datos
        {
            return $listadoConsumos->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_consumo){
        $this->db->where("id_consumo",$id_consumo);
        if($this->db->delete("consumo")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_consumo){
            $this->db->where("id_consumo",$id_consumo);
            $consumo=$this->db->get("consumo");
            if($consumo->num_rows()>0){
                return $consumo->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_consumo,$datos){
        $this->db->where("id_consumo",$id_consumo);
        return $this->db->update ('consumo',$datos);
        }

 } // Cierre de la clase
 ?>

