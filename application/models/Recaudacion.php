<?php
 class Recaudacion extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("recaudacion",$datos);
    }
    //funcion para consultar ucrecaudacions
    function obtenerTodos(){
        $this->db->join("socio","socio.id_soc = recaudacion.fk_id_soc");
        $listadorecaudacions=$this->db->get("recaudacion");
        if($listadorecaudacions->num_rows()>0)//si hay datos
        {
            return $listadorecaudacions->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_rec){
        $this->db->where("id_rec",$id_rec);
        if($this->db->delete("recaudacion")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_rec){
            $this->db->where("id_rec",$id_rec);
            $recaudacion=$this->db->get("recaudacion");
            if($recaudacion->num_rows()>0){
                return $recaudacion->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_rec,$datos){
        $this->db->where("id_rec",$id_rec);
        return $this->db->update ('recaudacion',$datos);
        }

 } // Cierre de la clase
 ?>
