<?php
 class Configuracion extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("configuracion",$datos);
    }
    //funcion para consultar uconfiguracions
    function obtenerTodos(){
        $listadoConfiguracions=$this->db->get("configuracion");
        if($listadoConfiguracions->num_rows()>0)//si hay datos
        {
            return $listadoConfiguracions->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_con){
        $this->db->where("id_con",$id_con);
        if($this->db->delete("configuracion")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_con){
            $this->db->where("id_con",$id_con);
            $configuracion=$this->db->get("configuracion");
            if($configuracion->num_rows()>0){
                return $configuracion->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_con,$datos){
        $this->db->where("id_con",$id_con);
        return $this->db->update ('configuracion',$datos);
        }

 } // Cierre de la clase
 ?>
