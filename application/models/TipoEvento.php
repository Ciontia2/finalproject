<?php
 class tipoEvento extends CI_Model
 {
    function __construct()
    {
        parent::__construct();
    }
    //funcion para insertar un instructor
    function insertar($datos)
    {
        return $this->db->insert("tipo_evento",$datos);
    }
    //funcion para consultar uctipo_eventos
    function obtenerTodos(){
        $this->db->join("usuario","usuario.id_usu = tipo_evento.id_te");
        $listadotipo_eventos=$this->db->get("tipo_evento");
        if($listadotipo_eventos->num_rows()>0)//si hay datos
        {
            return $listadotipo_eventos->result();
        }else{//no hay datos
            return false;
        }
    }
    // borrar instructor
        function borrar($id_te){
        $this->db->where("id_te",$id_te);
        if($this->db->delete("tipo_evento")){
            return true;
        }else{
            return false;
        }
    }
        // funcion consultar guardiaespecifico
        function obtenerPorId($id_te){
            $this->db->where("id_te",$id_te);
            $tipo_evento=$this->db->get("tipo_evento");
            if($tipo_evento->num_rows()>0){
                return $tipo_evento->row();
            }
                return false;
            }

        // actualizar un guardia
        function actualizar($id_te,$datos){
        $this->db->where("id_te",$id_te);
        return $this->db->update ('tipo_evento',$datos);
        }

 } // Cierre de la clase
 ?>
