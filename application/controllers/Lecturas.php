<?php
class Lecturas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Cceectura
        $this->load->model('Lectura');
        $this->load->model('Historial_propietario');
        $this->load->model('Consumo');
    }

    // Función de index o listado
    public function index()
    {
        $data['lecturas'] = $this->Lectura->obtenerTodos();
        $this->load->view('header');
        $this->load->view('lecturas/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data['historial'] = $this->Historial_propietario->obtenerTodos();
        $data['Consumo'] = $this->Consumo->obtenerTodos();
        $this->load->view('header');
        $this->load->view('lecturas/nuevo',$data);
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo cceectura
    public function guardar()
    {
        $datosNuevoLectura = array(
            "anio_lec" => $this->input->post('anio_lec'),
            "mes_lec" => $this->input->post('mes_lec'),
            "estado_lec" => $this->input->post('estado_lec'),
            "lectura_anterior_lec" => $this->input->post('lectura_anterior_lec'),
            "lectura_actual_lec" => $this->input->post('lectura_actual_lec'),
            "fecha_creacion_lec" => $this->input->post('fecha_creacion_lec'),
            "fecha_actualizacion_lec" => $this->input->post('fecha_actualizacion_lec'),
            "fk_id_his" => $this->input->post('fk_id_his'),
            "fk_id_consumo" => $this->input->post('fk_id_consumo'),

        );
        if ($this->Lectura->insertar($datosNuevoLectura)) {
            redirect('lecturas/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un cceectura
    public function eliminar($id_lec)
    {
        if ($this->Lectura->borrar($id_lec)) {
            redirect('lecturas/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el cceectura respectivo
    public function editar($id_lec)
    {
        $data["lecturaEditar"] = $this->Lectura->obtenerPorId($id_lec);
        $data['historial'] = $this->Historial_propietario->obtenerTodos();
        $data['Consumo'] = $this->Consumo->obtenerTodos();
        $this->load->view('header');
        $this->load->view('lecturas/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "anio_lec" => $this->input->post('anio_lec'),
            "mes_lec" => $this->input->post('mes_lec'),
            "estado_lec" => $this->input->post('estado_lec'),
            "lectura_anterior_lec" => $this->input->post('lectura_anterior_lec'),
            "lectura_actual_lec" => $this->input->post('lectura_actual_lec'),
            "fecha_creacion_lec" => $this->input->post('fecha_creacion_lec'),
            "fecha_actualizacion_lec" => $this->input->post('fecha_actualizacion_lec'),
            "fk_id_his" => $this->input->post('fk_id_his'),
            "fk_id_consumo" => $this->input->post('fk_id_consumo'),
        );
        $id_lec = $this->input->post("id_lec");
        if ($this->Lectura->actualizar($id_lec, $datosEditados)) {
            redirect("lecturas/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
