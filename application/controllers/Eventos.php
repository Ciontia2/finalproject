<?php
class Eventos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Ccevento
        $this->load->model('Evento');
        $this->load->model('tipoEvento');
    }

    // Función de index o listado
    public function index()
    {
        $data['tbleventos'] = $this->Evento->obtenerTodos();
        $this->load->view('header');
        $this->load->view('eventos/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data['eventos'] = $this->tipoEvento->obtenerTodos();
        $this->load->view('header');
        $this->load->view('eventos/nuevo',$data);
        $this->load->view('footer');
    }

    public function editar($id_eve)
    {
        $data["eventoEditar"] = $this->Evento->obtenerPorId($id_eve);
        $this->load->view('header');
        $this->load->view('eventos/editar', $data);
        $this->load->view('footer');
    }
    // Proceso para guardar un nuevo ccevento
    public function guardar()
    {
        $datosNuevoEvento = array(
          "descripcion_eve" => $this->input->post('descripcion_eve'),
          "fecha_hora_eve" => $this->input->post('fecha_hora_eve'),
          "lugar_eve" => $this->input->post('lugar_eve'),
          "fk_id_te" => $this->input->post('fk_id_te')
        );
        if ($this->Evento->insertar($datosNuevoEvento)) {
            redirect('eventos/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un ccevento
    public function eliminar($id_eve)
    {
        if ($this->Evento->borrar($id_eve)) {
            redirect('eventos/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el ccevento respectivo


    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "descripcion_eve" => $this->input->post('descripcion_eve'),
          "fecha_hora_eve" => $this->input->post('fecha_hora_eve'),
          "lugar_eve" => $this->input->post('lugar_eve'),
          "fk_id_te" => $this->input->post('fk_id_te')
        );
        $id_eve = $this->input->post("id_eve");
        if ($this->Evento->actualizar($id_eve, $datosEditados)) {
            redirect("eventos/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
?>

