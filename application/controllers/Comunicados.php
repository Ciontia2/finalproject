<?php
class Comunicados extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Comunicado
        $this->load->model('Comunicado');
    }

    // Función de index o listado
    public function index()
    {
        $data['comunicados'] = $this->Comunicado->obtenerTodos();
        $this->load->view('header');
        $this->load->view('comunicados/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('comunicados/nuevo');
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo comunicado
    public function guardar()
    {
        $datosNuevoComunicado = array(
            "fecha_com" => $this->input->post('fecha_com'),
            "mensaje_com" => $this->input->post('mensaje_com'),
            "actualizacion_com" => $this->input->post('actualizacion_com'),
            "creacion_com" => $this->input->post('creacion_com')
        );
        if ($this->Comunicado->insertar($datosNuevoComunicado)) {
            redirect('comunicados/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un comunicado
    public function eliminar($id_com)
    {
        if ($this->Comunicado->borrar($id_com)) {
            redirect('comunicados/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el comunicado respectivo
    public function editar($id_com)
    {
        $data["comunicadoEditar"] = $this->Comunicado->obtenerPorId($id_com);
        $this->load->view('header');
        $this->load->view('comunicados/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "fecha_com" => $this->input->post('fecha_com'),
            "mensaje_com" => $this->input->post('mensaje_com'),
            "actualizacion_com" => $this->input->post('actualizacion_com'),
            "creacion_com" => $this->input->post('creacion_com'),
        );
        $id_com = $this->input->post("id_com");
        if ($this->Comunicado->actualizar($id_com, $datosEditados)) {
            redirect("comunicados/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
?>
