<?php
class Socios extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Socio');
        $this->load->model('Usuario');
    }

    // Función de index o listado
    public function index()
    {
        $data["tblsocioc"] = $this->Socio->obtenerTodos();
        $this->load->view('header');
        $this->load->view('socios/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data["usuario"] = $this->Usuario->obtenerTodos();
        $this->load->view('header');
        $this->load->view('socios/nuevo', $data);
        $this->load->view('footer');
    }
    public function editar($id_soc)
    {
        $data["socio"] = $this->Socio->obtenerPorId($id_soc);
        $data["usuario"] = $this->Usuario->obtenerTodos();

        $this->load->view('header');
        $this->load->view('socios/editar', $data);
        $this->load->view('footer');
    }
    public function guardar()
    {
        $datos = array(
            "tipo_soc" => $this->input->post('tipo_soc'),
            "identificacion_soc" => $this->input->post('identificacion_soc'),
            "primer_apellido_soc" => $this->input->post('primer_apellido_soc'),
            "segundo_apellido_soc" => $this->input->post('segundo_apellido_soc'),
            "nombres_soc" => $this->input->post('nombres_soc'),
            "email_soc" => $this->input->post('email_soc'),
            "telefono_soc" => $this->input->post('telefono_soc'),
            "direccion_soc" => $this->input->post('direccion_soc'),
            "fecha_nacimiento_soc" => $this->input->post('fecha_nacimiento_soc'),
            "discapacidad_soc" => $this->input->post('discapacidad_soc'),
            "estado_soc" => $this->input->post('estado_soc'),
            "fk_id_usu" => $this->input->post('fk_id_usu'),
        );
        print_r($datos);
        if ($this->Socio->insertar($datos)) {
            redirect('Socios/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un comunicado
    public function eliminar($id_soc)
    {
        if ($this->Socio->borrar($id_soc)) {
            redirect('Socios/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el comunicado respectivo


    // Proceso de actualización
    public function Actualizar()
    {
        $datosEditados = array(
            "tipo_soc" => $this->input->post('tipo_soc'),
            "identificacion_soc" => $this->input->post('identificacion_soc'),
            "primer_apellido_soc" => $this->input->post('primer_apellido_soc'),
            "segundo_apellido_soc" => $this->input->post('segundo_apellido_soc'),
            "nombres_soc" => $this->input->post('nombres_soc'),
            "email_soc" => $this->input->post('email_soc'),
            "telefono_soc" => $this->input->post('telefono_soc'),
            "direccion_soc" => $this->input->post('direccion_soc'),
            "fecha_nacimiento_soc" => $this->input->post('fecha_nacimiento_soc'),
            "discapacidad_soc" => $this->input->post('discapacidad_soc'),
            "estado_soc" => $this->input->post('estado_soc'),
            "fk_id_usu" => $this->input->post('fk_id_usu'),
        );
        $id_soc = $this->input->post("id_soc");
        if ($this->Socio->actualizar($id_soc, $datosEditados)) {
            redirect("Socios/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
