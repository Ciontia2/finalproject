<?php
class Configuracions extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Cconfiguracion
        $this->load->model('Configuracion');
    }

    // Función de index o listado
    public function index()
    {
        $data['configuracions'] = $this->Configuracion->obtenerTodos();
        $this->load->view('header');
        $this->load->view('configuracions/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('configuracions/nuevo');
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo cconfiguracion
    public function guardar()
    {
        $datosNuevoConfiguracion = array(
            "nombre_con" => $this->input->post('nombre_con'),
            "ruc_con" => $this->input->post('ruc_con'),
            "logo_con" => $this->input->post('logo_con'),
            "telefono_con" => $this->input->post('telefono_con'),
            "direccion_con" => $this->input->post('direccion_con'),
            "email_con" => $this->input->post('email_con'),
            "servidor_con" => $this->input->post('servidor_con'),
            "puerto_con" => $this->input->post('puerto_con'),
            "password_con" => $this->input->post('password_con'),
            "creacion_con" => $this->input->post('creacion_con'),
            "actualizacion_con" => $this->input->post('actualizacion_con'),
            "anio_inicial_con" => $this->input->post('anio_inicial_con'),
            "mes_inicial_con" => $this->input->post('mes_inicial_con')
        );
        if ($this->Configuracion->insertar($datosNuevoConfiguracion)) {
            redirect('configuracions/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un cconfiguracion
    public function eliminar($id_con)
    {
        if ($this->Configuracion->borrar($id_con)) {
            redirect('configuracions/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el cconfiguracion respectivo
    public function editar($id_con)
    {
        $data["configuracionEditar"] = $this->Configuracion->obtenerPorId($id_con);
        $this->load->view('header');
        $this->load->view('configuracions/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "nombre_con" => $this->input->post('nombre_con'),
            "ruc_con" => $this->input->post('ruc_con'),
            "logo_con" => $this->input->post('logo_con'),
            "telefono_con" => $this->input->post('telefono_con'),
            "direccion_con" => $this->input->post('direccion_con'),
            "email_con" => $this->input->post('email_con'),
            "servidor_con" => $this->input->post('servidor_con'),
            "puerto_con" => $this->input->post('puerto_con'),
            "password_con" => $this->input->post('password_con'),
            "creacion_con" => $this->input->post('creacion_con'),
            "actualizacion_con" => $this->input->post('actualizacion_con'),
            "anio_inicial_con" => $this->input->post('anio_inicial_con'),
            "mes_inicial_con" => $this->input->post('mes_inicial_con'),
        );
        $id_con = $this->input->post("id_con");
        if ($this->Configuracion->actualizar($id_con, $datosEditados)) {
            redirect("configuracions/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
?>
