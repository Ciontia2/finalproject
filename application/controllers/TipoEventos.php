<?php
class TipoEventos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // $this->load->model('Socio');
        $this->load->model('tipoEvento');
    }

    // Función de index o listado
    public function index()
    {
        $data["tblEvento"] = $this->tipoEvento->obtenerTodos();
        $this->load->view('header');
        $this->load->view('tipoEvento/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('tipoEvento/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_te)
    {
        $data["Tipoevento"] = $this->tipoEvento->obtenerPorId($id_te);

        $this->load->view('header');
        $this->load->view('tipoEvento/editar', $data);
        $this->load->view('footer');
    }
    public function guardar()
    {
        $datos = array(
            "nombre_te" => $this->input->post('nombre_te'),
            "estado_te" => $this->input->post('estado_te'),
            "creacion_te" => $this->input->post('creacion_te'),
            "actualizacion_te" => $this->input->post('actualizacion_te'),
        );
        print_r($datos);
        if ($this->tipoEvento->insertar($datos)) {
            redirect('TipoEventos/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un comunicado
    public function eliminar($id_te)
    {
        if ($this->tipoEvento->borrar($id_te)) {
            redirect('TipoEventos/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el comunicado respectivo


    // Proceso de actualización
    public function Actualizar()
    {
        $datosEditados = array(
            "nombre_te" => $this->input->post('nombre_te'),
            "estado_te" => $this->input->post('estado_te'),
            "creacion_te" => $this->input->post('creacion_te'),
            "actualizacion_te" => $this->input->post('actualizacion_te'),
        );
        $id_te = $this->input->post("id_te");
        if ($this->tipoEvento->actualizar($id_te , $datosEditados)) {
            redirect("TipoEventos/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
