<?php
class Medidors extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Ccemedidor
        $this->load->model('Medidor');
        $this->load->model('Ruta');
        $this->load->model('Tarifa');
    }

    // Función de index o listado
    public function index()
    {
        $data['medidors'] = $this->Medidor->obtenerTodos();
        $this->load->view('header');
        $this->load->view('medidors/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data['ruta'] = $this->Ruta->obtenerTodos();
        $data['tarifa'] = $this->Tarifa->obtenerTodos();
        $this->load->view('header');
        $this->load->view('medidors/nuevo',$data);
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo ccemedidor
    public function guardar()
    {
        $datosNuevoMedidor = array(
            "numero_med" => $this->input->post('numero_med'),
            "serie_med" => $this->input->post('serie_med'),
            "marca_med" => $this->input->post('marca_med'),
            "observacion_med" => $this->input->post('observacion_med'),
            "estado_med" => $this->input->post('estado_med'),
            "foto_med" => $this->input->post('foto_med'),
            "creacion_med" => $this->input->post('creacion_med'),
            "actualizacion_med" => $this->input->post('actualizacion_med'),
            "lectura_inicial_med" => $this->input->post('lectura_inicial_med'),
            "fk_id_tar" => $this->input->post('fk_id_tar'),
            "fk_id_rut" => $this->input->post('fk_id_rut')
        );
        if ($this->Medidor->insertar($datosNuevoMedidor)) {
            redirect('medidors/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un ccemedidor
    public function eliminar($id_med)
    {
        if ($this->Medidor->borrar($id_med)) {
            redirect('medidors/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el ccemedidor respectivo
    public function editar($id_med)
    {
        $data["medidorEditar"] = $this->Medidor->obtenerPorId($id_med);
        $data['ruta'] = $this->Ruta->obtenerTodos();
        $data['tarifa'] = $this->Tarifa->obtenerTodos();
        $this->load->view('header');
        $this->load->view('medidors/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "numero_med" => $this->input->post('numero_med'),
            "serie_med" => $this->input->post('serie_med'),
            "marca_med" => $this->input->post('marca_med'),
            "observacion_med" => $this->input->post('observacion_med'),
            "estado_med" => $this->input->post('estado_med'),
            "foto_med" => $this->input->post('foto_med'),
            "creacion_med" => $this->input->post('creacion_med'),
            "actualizacion_med" => $this->input->post('actualizacion_med'),
            "lectura_inicial_med" => $this->input->post('lectura_inicial_med'),
            "fk_id_tar" => $this->input->post('fk_id_tar'),
            "fk_id_rut" => $this->input->post('fk_id_rut')
        );
        $id_med = $this->input->post("id_med");
        if ($this->Medidor->actualizar($id_med, $datosEditados)) {
            redirect("medidors/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
