<?php
  class Impuestos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Cimpuesto
        $this->load->model('Impuesto');
    }

    // Función de index o listado
    public function index()
    {
        $data['impuestos'] = $this->Impuesto->obtenerTodos();
        $this->load->view('header');
        $this->load->view('impuestos/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('impuestos/nuevo');
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo cimpuesto
    public function guardar()
    {
        $datosNuevoImpuesto = array(
            "nombre_imp" => $this->input->post('nombre_imp'),
            "descripcion_imp" => $this->input->post('descripcion_imp'),
            "porcentaje_imp" => $this->input->post('porcentaje_imp'),
            "retencion_imp" => $this->input->post('retencion_imp'),
            "estado_imp" => $this->input->post('estado_imp'),
            "creacion_imp" => $this->input->post('creacion_imp'),
            "actualizacion_imp" => $this->input->post('actualizacion_imp')
        );
        if ($this->Impuesto->insertar($datosNuevoImpuesto)) {
            redirect('impuestos/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un cimpuesto
    public function eliminar($id_imp)
    {
        if ($this->Impuesto->borrar($id_imp)) {
            redirect('impuestos/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el cimpuesto respectivo
    public function editar($id_imp)
    {
        $data["impuestoEditar"] = $this->Impuesto->obtenerPorId($id_imp);
        $this->load->view('header');
        $this->load->view('impuestos/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
          "nombre_imp" => $this->input->post('nombre_imp'),
          "descripcion_imp" => $this->input->post('descripcion_imp'),
          "porcentaje_imp" => $this->input->post('porcentaje_imp'),
          "retencion_imp" => $this->input->post('retencion_imp'),
          "estado_imp" => $this->input->post('estado_imp'),
          "creacion_imp" => $this->input->post('creacion_imp'),
          "actualizacion_imp" => $this->input->post('actualizacion_imp'),
        );
        $id_imp = $this->input->post("id_imp");
        if ($this->Impuesto->actualizar($id_imp, $datosEditados)) {
            redirect("impuestos/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
?>
