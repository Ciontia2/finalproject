<?php
class Recaudaciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Socio');
        $this->load->model('Recaudacion');
    }

    // Función de index o listado
    public function index()
    {
        $data["tblRecaudacion"] = $this->Recaudacion->obtenerTodos();
        $this->load->view('header');
        $this->load->view('recaudaciones/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data["Socio"] = $this->Socio->obtenerTodos();
        $this->load->view('header');
        $this->load->view('recaudaciones/nuevo', $data);
        $this->load->view('footer');
    }
    public function editar($id_rec)
    {
        $data["Recaudacion"] = $this->Recaudacion->obtenerPorId($id_rec);
        $data["Socio"] = $this->Socio->obtenerTodos();

        $this->load->view('header');
        $this->load->view('recaudaciones/editar', $data);
        $this->load->view('footer');
    }
    public function guardar()
    {
        $datos = array(
            "numero_factura_rec" => $this->input->post('numero_factura_rec'),
            "numero_autorizacion_rec" => $this->input->post('numero_autorizacion_rec'),
            "fecha_hora_autorizacion_rec" => $this->input->post('fecha_hora_autorizacion_rec'),
            "ambiente_rec" => $this->input->post('ambiente_rec'),
            "emision_rev" => $this->input->post('emision_rev'),
            "clave_acceso_rec" => $this->input->post('clave_acceso_rec'),
            "email_rec" => $this->input->post('email_rec'),
            "observacion_rec" => $this->input->post('observacion_rec'),
            "nombre_rec" => $this->input->post('nombre_rec'),
            "identificacion_rec" => $this->input->post('identificacion_rec'),
            "direccion_rec" => $this->input->post('direccion_rec'),
            "estado_rec" => $this->input->post('estado_rec'),
            "fecha_emision_rec" => $this->input->post('fecha_emision_rec'),
            "fecha_creacion_rec" => $this->input->post('fecha_creacion_rec'),
            "fecha_actualizacion_rec" => $this->input->post('fecha_actualizacion_rec'),
            "fk_id_soc" => $this->input->post('fk_id_soc'),
        );
        print_r($datos);
        if ($this->Recaudacion->insertar($datos)) {
            redirect('Recaudaciones/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un comunicado
    public function eliminar($id_rec)
    {
        if ($this->Recaudacion->borrar($id_rec)) {
            redirect('Recaudaciones/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el comunicado respectivo


    // Proceso de actualización
    public function Actucalizar()
    {
        $datosEditados = array(
            "numero_factura_rec" => $this->input->post('numero_factura_rec'),
            "numero_autorizacion_rec" => $this->input->post('numero_autorizacion_rec'),
            "fecha_hora_autorizacion_rec" => $this->input->post('fecha_hora_autorizacion_rec'),
            "ambiente_rec" => $this->input->post('ambiente_rec'),
            "emision_rev" => $this->input->post('emision_rev'),
            "clave_acceso_rec" => $this->input->post('clave_acceso_rec'),
            "email_rec" => $this->input->post('email_rec'),
            "observacion_rec" => $this->input->post('observacion_rec'),
            "nombre_rec" => $this->input->post('nombre_rec'),
            "identificacion_rec" => $this->input->post('identificacion_rec'),
            "direccion_rec" => $this->input->post('direccion_rec'),
            "estado_rec" => $this->input->post('estado_rec'),
            "fecha_emision_rec" => $this->input->post('fecha_emision_rec'),
            "fecha_creacion_rec" => $this->input->post('fecha_creacion_rec'),
            "fecha_actualizacion_rec" => $this->input->post('fecha_actualizacion_rec'),
            "fk_id_soc" => $this->input->post('fk_id_soc'),);
        $id_rec = $this->input->post("id_rec");
        if ($this->Recaudacion->actualizar($id_rec, $datosEditados)) {
            redirect("Recaudaciones/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
