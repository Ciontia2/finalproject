<?php
class Usuarios extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Perfil');
        $this->load->model('Usuario');
    }

    // Función de index o listado
    public function index()
    {
        $data["tblUsuario"]= $this->Usuario->obtenerTodos();
        $this->load->view('header');
        $this->load->view('usuarios/index',$data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data["perfil"]= $this->Perfil->obtenerTodos();
        $this->load->view('header');
        $this->load->view('usuarios/nuevo',$data);
        $this->load->view('footer');
    }


    public function guardar()
    {
        $datos = array(
            "apellido_usu" => $this->input->post('apellido_usu'),
            "nombre_usu" => $this->input->post('nombre_usu'),
            "email_usu" => $this->input->post('email_usu'),
            "password_usu" => $this->input->post('password_usu'),
            "estado_usu" => $this->input->post('estado_usu'),
            "fk_id_per" => $this->input->post('fk_id_per'),
        );
        print_r($datos);
        if ($this->Usuario->insertar($datos)) {
            redirect('Usuarios/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un comunicado
    public function eliminar($id_usu)
    {
        if ($this->Usuario->borrar($id_usu)) {
            redirect('Usuarios/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el comunicado respectivo
    public function editar($id_usu)
    {
        $data["editarUsu"] = $this->Usuario->obtenerPorId($id_usu);
        $data["perfil"]= $this->Perfil->obtenerTodos();

        $this->load->view('header');
        $this->load->view('usuarios/editar',$data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesoActualizar()
    {
        $datosEditados = array(
            "apellido_usu" => $this->input->post('apellido_usu'),
            "nombre_usu" => $this->input->post('nombre_usu'),
            "email_usu" => $this->input->post('email_usu'),
            "password_usu" => $this->input->post('password_usu'),
            "estado_usu" => $this->input->post('estado_usu'),
            "fk_id_per" => $this->input->post('fk_id_per'),
        );
        $id_usu = $this->input->post("id_usu");
        if ($this->Usuario->actualizar($id_usu, $datosEditados)) {
            redirect("Usuarios/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
?>
