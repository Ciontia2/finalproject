<?php
class Consumos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Consumo
        $this->load->model('Consumo');
    }

    // Función de index o listado
    public function index()
    {
        $data['consumos'] = $this->Consumo->obtenerTodos();
        $this->load->view('header');
        $this->load->view('consumos/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('consumos/nuevo');
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo consumo
    public function guardar()
    {
        $datosNuevoConsumo = array(
            "anio_consumo" => $this->input->post('anio_consumo'),
            "mes_consumo" => $this->input->post('mes_consumo'),
            "estado_consumo" => $this->input->post('estado_consumo'),
            "fecha_creacion_consumo" => $this->input->post('fecha_creacion_consumo'),
            "fecha_actualizacion_consumo" => $this->input->post('fecha_actualizacion_consumo'),
            "numero_mes_consumo" => $this->input->post('numero_mes_consumo'),
            "fecha_vencimiento_consumo" => $this->input->post('fecha_vencimiento_consumo'),

        );
        if ($this->Consumo->insertar($datosNuevoConsumo)) {
            redirect('consumos/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un consumo
    public function eliminar($id_consumo)
    {
        if ($this->Consumo->borrar($id_consumo)) {
            redirect('consumos/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el consumo respectivo
    public function editar($id_consumo)
    {
        $data["consumoEditar"] = $this->Consumo->obtenerPorId($id_consumo);
        $this->load->view('header');
        $this->load->view('consumos/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "anio_consumo" => $this->input->post('anio_consumo'),
            "mes_consumo" => $this->input->post('mes_consumo'),
            "estado_consumo" => $this->input->post('estado_consumo'),
            "fecha_creacion_consumo" => $this->input->post('fecha_creacion_consumo'),
            "fecha_actualizacion_consumo" => $this->input->post('fecha_actualizacion_consumo'),
            "numero_mes_consumo" => $this->input->post('numero_mes_consumo'),
            "fecha_vencimiento_consumo" => $this->input->post('fecha_vencimiento_consumo')
        );
        $id_consumo = $this->input->post("id_consumo");
        if ($this->Consumo->actualizar($id_consumo, $datosEditados)) {
            redirect("consumos/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
