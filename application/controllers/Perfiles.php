<?php 
class Perfiles extends CI_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model("Perfil");
    }
    public function index(){
        $data["tablaPer"] = $this->Perfil->obtenerTodos();
        $this->load->view("header");
        $this->load->view("perfil/index",$data);
        $this->load->view("footer");
    }
    public function nuevo(){
        $this->load->view("header");
        $this->load->view("perfil/nuevo");
        $this->load->view("footer");
    }
    public function editar($id_per){
        $data["editPer"] = $this->Perfil->obtenerPorId($id_per);
        $this->load->view("header");
        $this->load->view("perfil/editar",$data);
        $this->load->view("footer");
    }
    function guardar()
    {
        $datos = array(
            "nombre_per" => $this->input->post("nombre_per"),
            "descripcion_per" => $this->input->post("descripcion_per"),
            "estado_per" => $this->input->post("estado_per"),
            "creacion_per" => $this->input->post("creacion_per"),
            "actualizacion_per" => $this->input->post("actualizacion_per"),

        );
        if ($this->Perfil->insertar($datos)) {
            // $this->session->set_flashdata("correcto", "Registro insertado correctamente.");
            redirect("/Perfiles/index");
        } else {
            echo "ocurrio un error";
        }
    }
    function eliminar($id_per)
    {
        if ($this->Perfil->borrar($id_per)) {
            // $this->session->set_flashdata("eliminar", "Registro eliminado correctamente.");
            redirect("/Perfiles/index");
        } else {
            echo "hubo un error al eliminar el registro";
        }
    }
    function Actualizar()
    {
        $datos = array(
            "nombre_per" => $this->input->post("nombre_per"),
            "descripcion_per" => $this->input->post("descripcion_per"),
            "estado_per" => $this->input->post("estado_per"),
            "creacion_per" => $this->input->post("creacion_per"),
            "actualizacion_per" => $this->input->post("actualizacion_per"),
        );
        $id_per = $this->input->post("id_per");
        if ($this->Perfil->actualizar($id_per, $datos)) {
            // $this->session->set_flashdata("actualizar", "Registro Actualizado correctamente.");
            redirect("/Perfiles/index");
        } else {
            echo "error al actualizar";
            // $this->session->set_flashdata("eliminar", "error algo salio mal al actualizar.");
        }
    }

}


?>