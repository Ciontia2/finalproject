<?php
class Excedentes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Ccexcedente
        $this->load->model('Excedente');
        $this->load->model('Tarifa');
    }

    // Función de index o listado
    public function index()
    {
        $data['excedentes'] = $this->Excedente->obtenerTodos();
        $this->load->view('header');
        $this->load->view('excedentes/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data['tarifa'] = $this->Tarifa->obtenerTodos();
        $this->load->view('header');
        $this->load->view('excedentes/nuevo',$data);
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo ccexcedente
    public function guardar()
    {
        $datosNuevoExcedente = array(
            "limite_minimo_ex" => $this->input->post('limite_minimo_ex'),
            "limite_maximo_ex" => $this->input->post('limite_maximo_ex'),
            "tarifa_ex" => $this->input->post('tarifa_ex'),
            "fecha_actualizacion_ex" => $this->input->post('fecha_actualizacion_ex'),
            "fecha_creacion_ex" => $this->input->post('fecha_creacion_ex'),
            "id_tar" => $this->input->post('id_tar')
        );
        if ($this->Excedente->insertar($datosNuevoExcedente)) {
            redirect('excedentes/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un ccexcedente
    public function eliminar($id_ex)
    {
        if ($this->Excedente->borrar($id_ex)) {
            redirect('excedentes/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el ccexcedente respectivo
    public function editar($id_ex)
    {
        $data["excedenteEditar"] = $this->Excedente->obtenerPorId($id_ex);
        $data['tarifa'] = $this->Tarifa->obtenerTodos();

        $this->load->view('header');
        $this->load->view('excedentes/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "limite_minimo_ex" => $this->input->post('limite_minimo_ex'),
            "limite_maximo_ex" => $this->input->post('limite_maximo_ex'),
            "tarifa_ex" => $this->input->post('tarifa_ex'),
            "fecha_actualizacion_ex" => $this->input->post('fecha_actualizacion_ex'),
            "fecha_creacion_ex" => $this->input->post('fecha_creacion_ex'),
            "id_tar" => $this->input->post('id_tar')

        );
        $id_ex = $this->input->post("id_ex");
        if ($this->Excedente->actualizar($id_ex, $datosEditados)) {
            redirect("excedentes/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
