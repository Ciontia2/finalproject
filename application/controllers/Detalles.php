<?php
class Detalles extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Detalle
        $this->load->model('Detalle');
        $this->load->model('Recaudacion');
        $this->load->model('Lectura');
    }

    // Función de index o listado
    public function index()
    {
        $data['detalles'] = $this->Detalle->obtenerTodos();
        $this->load->view('header');
        $this->load->view('detalles/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data['Recaudacion'] = $this->Recaudacion->obtenerTodos();
        $data['Lectura'] = $this->Lectura->obtenerTodos();
        $this->load->view('header');
        $this->load->view('detalles/nuevo',$data);
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo detalle
    public function guardar()
    {
        $datosNuevoDetalle = array(
            "cantidad_det" => $this->input->post('cantidad_det'),
            "detalle_det" => $this->input->post('detalle_det'),
            "valor_unitario_det" => $this->input->post('valor_unitario_det'),
            "subtotal_det" => $this->input->post('subtotal_det'),
            "iva_det" => $this->input->post('iva_det'),
            "fk_id_lec" => $this->input->post('fk_id_lec'),
            "fk_id_rec" => $this->input->post('fk_id_rec')
        );
        if ($this->Detalle->insertar($datosNuevoDetalle)) {
            redirect('detalles/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un detalle
    public function eliminar($id_det)
    {
        if ($this->Detalle->borrar($id_det)) {
            redirect('detalles/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el detalle respectivo
    public function editar($id_det)
    {
        $data["detalleEditar"] = $this->Detalle->obtenerPorId($id_det);
        $data['Recaudacion'] = $this->Recaudacion->obtenerTodos();
        $data['Lectura'] = $this->Lectura->obtenerTodos();
        $this->load->view('header');
        $this->load->view('detalles/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "cantidad_det" => $this->input->post('cantidad_det'),
            "detalle_det" => $this->input->post('detalle_det'),
            "valor_unitario_det" => $this->input->post('valor_unitario_det'),
            "subtotal_det" => $this->input->post('subtotal_det'),
            "iva_det" => $this->input->post('iva_det'),
            "fk_id_lec" => $this->input->post('fk_id_lec'),
            "fk_id_rec" => $this->input->post('fk_id_rec')
        );
        $id_det = $this->input->post("id_det");
        if ($this->Detalle->actualizar($id_det, $datosEditados)) {
            redirect("detalles/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
?>
