<?php
class Historial_propietarios extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Cistorial_propietario
        $this->load->model('Historial_propietario');
        $this->load->model('Socio');
        $this->load->model('Medidor');
    }

    // Función de index o listado
    public function index()
    {
        $data['historial_propietarios'] = $this->Historial_propietario->obtenerTodos();
        $this->load->view('header');
        $this->load->view('historial_propietarios/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data['socio'] = $this->Socio->obtenerTodos();
        $data['meditor'] = $this->Medidor->obtenerTodos();
        $this->load->view('header');
        $this->load->view('historial_propietarios/nuevo', $data);
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo cistorial_propietario
    public function guardar()
    {
        $datosNuevoHistorial_propietario = array(
            "actualizacion_his" => $this->input->post('actualizacion_his'),
            "estado_his" => $this->input->post('estado_his'),
            "observacion_his" => $this->input->post('observacion_his'),
            "fecha_cambio_his" => $this->input->post('fecha_cambio_his'),
            "creacion_his" => $this->input->post('creacion_his'),
            "propietario_actual_his" => $this->input->post('propietario_actual_his'),
            "fk_id_soc" => $this->input->post('fk_id_soc'),
            "fk_id_med" => $this->input->post('fk_id_med'),

        );
        if ($this->Historial_propietario->insertar($datosNuevoHistorial_propietario)) {
            redirect('historial_propietarios/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un cistorial_propietario
    public function eliminar($id_his)
    {
        if ($this->Historial_propietario->borrar($id_his)) {
            redirect('historial_propietarios/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el cistorial_propietario respectivo
    public function editar($id_his)
    {
        $data["historial_propietarioEditar"] = $this->Historial_propietario->obtenerPorId($id_his);
        $this->load->view('header');
        $this->load->view('historial_propietarios/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "actualizacion_his" => $this->input->post('actualizacion_his'),
            "estado_his" => $this->input->post('estado_his'),
            "observacion_his" => $this->input->post('observacion_his'),
            "fecha_cambio_his" => $this->input->post('fecha_cambio_his'),
            "creacion_his" => $this->input->post('creacion_his'),
            "propietario_actual_his" => $this->input->post('propietario_actual_his'),
            "fk_id_soc" => $this->input->post('fk_id_soc'),
            "fk_id_med" => $this->input->post('fk_id_med'),
        );
        $id_his = $this->input->post("id_his");
        if ($this->Historial_propietario->actualizar($id_his, $datosEditados)) {
            redirect("historial_propietarios/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
