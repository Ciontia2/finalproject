<?php
class Asistencias extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Cargar el modelo Ciasistencia
        $this->load->model('Asistencia');
        $this->load->model('Socio');
        $this->load->model('Evento');
    }

    // Función de index o listado
    public function index()
    {
        $data['asistencias'] = $this->Asistencia->obtenerTodos();
        $this->load->view('header');
        $this->load->view('asistencias/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $data['Socio'] = $this->Socio->obtenerTodos();
        $data['Evento'] = $this->Evento->obtenerTodos();

        $this->load->view('header');
        $this->load->view('asistencias/nuevo',$data);
        $this->load->view('footer');
    }

    // Proceso para guardar un nuevo ciasistencia
    public function guardar()
    {
        $datosNuevoAsistencia = array(
            "tipo_asi" => $this->input->post('tipo_asi'),
            "valor_asi" => $this->input->post('valor_asi'),
            "atraso_asi" => $this->input->post('atraso_asi'),
            "valor_atraso_asi" => $this->input->post('valor_atraso_asi'),
            "creacion_asi" => $this->input->post('creacion_asi'),
            "actualizacion_asi" => $this->input->post('actualizacion_asi'),
            "fk_id_soc" => $this->input->post('fk_id_soc'),
            "fk_id_eve" => $this->input->post('fk_id_eve'),
        );
        if ($this->Asistencia->insertar($datosNuevoAsistencia)) {
            redirect('asistencias/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un ciasistencia
    public function eliminar($id_asi)
    {
        if ($this->Asistencia->borrar($id_asi)) {
            redirect('asistencias/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el ciasistencia respectivo
    public function editar($id_asi)
    {
        $data["asistenciaEditar"] = $this->Asistencia->obtenerPorId($id_asi);
        $data['Socio'] = $this->Socio->obtenerTodos();
        $data['Evento'] = $this->Evento->obtenerTodos();
        $this->load->view('header');
        $this->load->view('asistencias/editar', $data);
        $this->load->view('footer');
    }

    // Proceso de actualización
    public function procesarActualizacion()
    {
        $datosEditados = array(
            "tipo_asi" => $this->input->post('tipo_asi'),
            "valor_asi" => $this->input->post('valor_asi'),
            "atraso_asi" => $this->input->post('atraso_asi'),
            "valor_atraso_asi" => $this->input->post('valor_atraso_asi'),
            "creacion_asi" => $this->input->post('creacion_asi'),
            "actualizacion_asi" => $this->input->post('actualizacion_asi'),
            "fk_id_soc" => $this->input->post('fk_id_soc'),
            "fk_id_eve" => $this->input->post('fk_id_eve'),
        );
        $id_asi = $this->input->post("id_asi");
        if ($this->Asistencia->actualizar($id_asi, $datosEditados)) {
            redirect("asistencias/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
