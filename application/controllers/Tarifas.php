<?php
class Tarifas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tarifa');
       
    }

    // Función de index o listado
    public function index()
    {
        $data["tblTarifa"] = $this->Tarifa->obtenerTodos();
        $this->load->view('header');
        $this->load->view('tarifas/index', $data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
        $this->load->view('header');
        $this->load->view('tarifas/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_tar)
    {
        $data["tarifa"] = $this->Tarifa->obtenerPorId($id_tar);
        $this->load->view('header');
        $this->load->view('tarifas/editar', $data);
        $this->load->view('footer');
    }
    public function guardar()
    {
        $datos = array(
            "nombre_tar" => $this->input->post('nombre_tar'),
            "descripcion_tar" => $this->input->post('descripcion_tar'),
            "estado_tar" => $this->input->post('estado_tar'),
            "m3_maximo_tar" => $this->input->post('m3_maximo_tar'),
            "tarifa_basica_tar" => $this->input->post('tarifa_basica_tar'),
            "tarifa_excedente_tar" => $this->input->post('tarifa_excedente_tar'),
            "valor_mora_tar" => $this->input->post('valor_mora_tar'),
            
        );
        print_r($datos);
        if ($this->Tarifa->insertar($datos)) {
            redirect('Tarifas/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un comunicado
    public function eliminar($id_tar)
    {
        if ($this->Tarifa->borrar($id_tar)) {
            redirect('Tarifas/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el comunicado respectivo


    // Proceso de actualización
    public function Actuaizar()
    {
        $datosEditados = array(
            "nombre_tar" => $this->input->post('nombre_tar'),
            "descripcion_tar" => $this->input->post('descripcion_tar'),
            "estado_tar" => $this->input->post('estado_tar'),
            "m3_maximo_tar" => $this->input->post('m3_maximo_tar'),
            "tarifa_basica_tar" => $this->input->post('tarifa_basica_tar'),
            "tarifa_excedente_tar" => $this->input->post('tarifa_excedente_tar'),
            "valor_mora_tar" => $this->input->post('valor_mora_tar'),
            );
        $id_tar = $this->input->post("id_tar");
        if ($this->Tarifa->actualizar($id_tar, $datosEditados)) {
            redirect("Tarifas/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
