<?php
class Rutas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Ruta');

    }

    // Función de index o listado
    public function index()
    {
        $data["tblRutas"]=$this->Ruta->obtenerTodos();
        $this->load->view('header');
        $this->load->view('rutas/index',$data);
        $this->load->view('footer');
    }

    // Función para renderizar la vista nuevo
    public function nuevo()
    {
       
        $this->load->view('header');
        $this->load->view('rutas/nuevo');
        $this->load->view('footer');
    }
    public function editar($id_rut)
    {
        $data["editRuta"] = $this->Ruta->obtenerPorId($id_rut);
        $this->load->view('header');
        $this->load->view('rutas/editar', $data);
        $this->load->view('footer');
    }
    public function guardar()
    {
        $datos = array(
            "nombre_rut" => $this->input->post('nombre_rut'),
            "descripcion_rut" => $this->input->post('descripcion_rut'),
            "estado_rut" => $this->input->post('estado_rut'),
           
        );
        print_r($datos);
        if ($this->Ruta->insertar($datos)) {
            redirect('Rutas/index');
        } else {
            echo "<h1>ERROR AL INSERTAR</h1>";
        }
    }

    // Función para eliminar un comunicado
    public function eliminar($id_rut)
    {
        if ($this->Ruta->borrar($id_rut)) {
            redirect('Rutas/index');
        } else {
            echo "Error al borrar";
        }
    }

    // Función para renderizar vista editar con el comunicado respectivo


    // Proceso de actualización
    public function Actualizar()
    {
        $datosEditados = array(
            "nombre_rut" => $this->input->post('nombre_rut'),
            "descripcion_rut" => $this->input->post('descripcion_rut'),
            "estado_rut" => $this->input->post('estado_rut'),
           
        );
        $id_rut = $this->input->post("id_rut");
        if ($this->Ruta->actualizar($id_rut, $datosEditados)) {
            redirect("Rutas/index");
        } else {
            echo "ERROR AL ACTUALIZAR";
        }
    }
}
